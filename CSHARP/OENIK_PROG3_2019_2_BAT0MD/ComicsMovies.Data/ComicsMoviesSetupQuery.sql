﻿IF OBJECT_ID('univerzum', 'U') IS NOT NULL DROP TABLE univerzum;

IF OBJECT_ID('szinesz', 'U') IS NOT NULL DROP TABLE szinesz;
IF OBJECT_ID('rendezo', 'U') IS NOT NULL DROP TABLE rendezo;

IF OBJECT_ID('film', 'U') IS NOT NULL DROP TABLE film;
IF OBJECT_ID('szereples', 'U') IS NOT NULL DROP TABLE szereples;


CREATE TABLE univerzum(
univerzumid numeric(1) NOT NULL,
nev VARCHAR(6) NOT NULL,
CONSTRAINT univerzum_pk PRIMARY KEY (univerzumid)
);

CREATE TABLE szinesz(
szineszid numeric(3) NOT NULL,
keresztnev VARCHAR(20) NOT NULL,
vezeteknev VARCHAR(20) NOT NULL,
nemzetiseg VARCHAR(20) NOT NULL,
sz_datum datetime NOT NULL,
szemszin VARCHAR(5) NOT NULL,
CONSTRAINT szinesz_pk PRIMARY KEY (szineszid)
);

CREATE TABLE rendezo(
rendezoid numeric(2) NOT NULL,
keresztnev VARCHAR(20) NOT NULL,
vezeteknev VARCHAR(20) NOT NULL,
nemzetiseg VARCHAR(20) NOT NULL,
sz_datum datetime NOT NULL,
fizetes numeric(20) NOT NULL,
CONSTRAINT rendezo_pk PRIMARY KEY (rendezoid)
);

CREATE TABLE film(
filmid numeric(2) NOT NULL,
cim VARCHAR(50) NOT NULL,
kijovetel datetime,
hossz numeric(3),
cselekmeny VARCHAR(350),
univerzumid numeric(1) NOT NULL,
rendezoid numeric(2),
bevetel numeric(20),
CONSTRAINT film_pk PRIMARY KEY (filmid),
CONSTRAINT univerzumid_fk FOREIGN KEY (univerzumid) REFERENCES univerzum(univerzumid),
CONSTRAINT rendezoid_fk FOREIGN KEY (rendezoid) REFERENCES rendezo(rendezoid)
);

CREATE TABLE szereples(
filmid numeric(2) NOT NULL,
szineszid numeric(3) NOT NULL,
szerep VARCHAR(50) NOT NULL,
CONSTRAINT szerep_fid_film_fid FOREIGN KEY (filmid) REFERENCES film(filmid),
CONSTRAINT szerep_szid_szinesz_szid FOREIGN KEY (szineszid) REFERENCES szinesz(szineszid),
CONSTRAINT szerep_pk PRIMARY KEY (filmid, szineszid)
);


INSERT INTO univerzum (univerzumid, nev) VALUES (1, 'DC');
INSERT INTO univerzum (univerzumid, nev) VALUES (2, 'Marvel');


INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (1, 'Chris','Evans','amerikai','1981.06.13', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (2, 'Hugo','Weaving','brit-ausztrál', '1960.04.04', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (3, 'Samuel L.','Jackson','amerikai', '1948.12.21', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (4, 'Hayley','Atwell','angol','1982.04.05', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (5, 'Sebastian','Stan','amerikai', '1982.08.13', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (6, 'Robert', 'Downey Jr.', 'amerikai', '1965.04.04', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (7, 'Gwyneth', 'Paltrow', 'amerikai', '1972.09.27', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (8, 'Terrence', 'Howard', 'amerikai', '1969.03.11', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (9, 'Jeff', 'Bridges', 'amerikai', '1949.12.04', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (10, 'Jon', 'Favreu', 'amerikai', '1966.10.19', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (11, 'Edward', 'Norton', 'amerikai', '1969.08.18', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (12, 'Liv', 'Tyler', 'amerikai', '1977.07.01', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (13, 'Mickey', 'Rourke', 'amerikai', '1952.09.16', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (14, 'Don', 'Cheadle', 'amerikai', '1964.11.29', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (15, 'Scarlett', 'Johansson', 'amerikai', '1984.11.22', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (16, 'Clark', 'Gregg', 'amerikai', '1962.04.02', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (17, 'Chris', 'Hemsworth', 'ausztrál', '1983.08.11', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (18, 'Anthony', 'Hopkins', 'walesi', '1937.12.31', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (19, 'Natalie', 'Portman', 'izraeli', '1981.06.09', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (20, 'Tom', 'Hiddleston', 'brit', '1981.02.09', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (21, 'Stellan', 'Skarsgard', 'svéd', '1951.06.13', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (22, 'Kat', 'Dennings', 'amerikai', '1986.06.13', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (23, 'Idris', 'Elba', 'brit', '1972.09.06', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (24, 'Jeremy', 'Renner', 'amerikai', '1971.01.07', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (25, 'Mark', 'Ruffalo', 'amerikai', '1967.11.22', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (26, 'Coby', 'Smulders', 'kanadai', '1982.04.03', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (27, 'Guy', 'Pearce', 'brit', '1967.10.05', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (28, 'Rebecca', 'Hall', 'brit', '1982.05.03', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (29, 'Anthony', 'Mackie', 'amerikai', '1978.09.23', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (30, 'Emily', 'VanCamp', 'kanadai', '1986.05.12', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (31, 'Chris', 'Pratt', 'amerikai', '1979.06.21', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (32, 'Vin', 'Diesel', 'amerikai', '1967.07.18', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (33, 'Bradley', 'Cooper', 'kanadai', '1975.01.05', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (34, 'Zoe', 'Saldana', 'dominikai', '1978.06.19', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (35, 'Dave', 'Bautista', 'amerikai','1969.01.18', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (36, 'Lee', 'Pace', 'amerikai','1979.03.25', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (37, 'Michael', 'Rooker', 'amerikai', '1955.02.06', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (38, 'Karen', 'Gillan', 'skót', '1987.11.28', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (39, 'James', 'Spader', 'amerikai', '1960.02.07', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (40, 'Aaron', 'Taylor-Johnson', 'angol', '1990.06.13', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (41, 'Elizabeth', 'Olsen', 'amerikai', '1989.02.16', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (42, 'Paul', 'Rudd', 'amerikai', '1969.04.06', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (43, 'Michael', 'Douglas', 'amerikai', '1944.09.25', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (44, 'Corey', 'Stoll', 'amerikai', '1976.03.14', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (45, 'Evangeline', 'Lilly', 'kanadai', '1979.08.03', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (46, 'Judy', 'Greer', 'amerikai', '1975.07.20', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (47, 'Chadwick', 'Boseman', 'amerikai', '1977.11.29', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (48, 'Paul', 'Bettany', 'angol','1971.05.27', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (49, 'Tom', 'Holland', 'brit', '1996.06.01', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (50, 'Marisa', 'Tomei', 'amerikai','1964.12.04', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (51, 'Benedict', 'Cumberbatch', 'brit', '1976.07.19', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (52, 'Chiwetel', 'Ejiofor', 'angol', '1977.07.10', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (53, 'Rachel', 'McAdams', 'brit', '1978.11.17', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (54, 'Benedict', 'Wong', 'brit', '1970.07.03', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (55, 'Mads', 'Mikkelsen', 'dán', '1965.11.22', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (56, 'Tilda', 'Swinton', 'brit', '1960.11.05', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (57, 'Pom', 'Klementieff', 'francia', '1986.05.03', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (58, 'Kurt', 'Russell', 'amerikai', '1951.03.17', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (59, 'Elizabeth', 'Debicki', 'ausztrál', '1990.08.24', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (60, 'Michael', 'Keaton', 'amerikai', '1951.09.05', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (61, 'Zendaya', 'Coleman', 'amerikai', '1996.09.01', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (62, 'Jacob', 'Batalon', 'hawaii', '1996.10.09', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (63, 'Laura', 'Harrier', 'amerikai', '1990.03.28', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (64, 'Cate', 'Blanchett', 'ausztrál', '1969.05.14', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (65, 'Jeff', 'Goldblum', 'amerikai', '1952.10.22', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (66, 'Tessa', 'Thompson', 'amerikai', '1983.10.03', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (67, 'Karl', 'Urban', 'új-zélandi', '1972.06.07', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (68, 'Lupita', 'Nyong-o', 'kenyai', '1983.03.01', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (69, 'Danai', 'Gurira', 'amerikai', '1978.02.14', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (70, 'Martin', 'Freeman', 'brit', '1971.10.08', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (71, 'Michael B.', 'Jordan', 'amerikai', '1987.02.09', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (72, 'Letitia', 'Wright', 'guyanai-angol', '1993.10.31', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (73, 'Josh', 'Brolin', 'amerikai', '1968.02.12', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (74, 'Brie', 'Larson', 'amerikai', '1989.10.01', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (75, 'Jake', 'Gyllenhaal', 'amerikai', '1980.12.19', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (76, 'Angelina', 'Jolie', 'amerikai', '1975.06.04', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (77, 'Richard', 'Madden', 'brit', '1986.06.18', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (78, 'Kit', 'Harington', 'brit', '1986.12.26', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (79, 'Salma', 'Hayek', 'mexikói', '1966.09.02', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (80, 'Henry', 'Cavill', 'brit', '1983.05.05', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (81, 'Amy', 'Adams', 'amerikai', '1974.08.20', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (82, 'Michael', 'Shannon', 'amerikai', '1974.08.07', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (83, 'Diane', 'Lane', 'amerikai', '1965.01.22', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (84, 'Ben', 'Affleck', 'amerikai', '1972.08.15', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (85, 'Jesse', 'Eisenberg', 'amerikai', '1983.10.05', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (86, 'Laurence', 'Fishburne', 'amerikai', '1961.07.30', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (87, 'Jeremy', 'Irons', 'brit', '1948.09.19', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (88, 'Gal', 'Gadot', 'izraeli', '1985.04.30', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (89, 'Jay O.', 'Sanders', 'amerikai', '1953.04.16', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (90, 'Will', 'Smith', 'amerikai', '1968.09.25', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (91, 'Jared', 'Leto', 'amerikai', '1971.12.26', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (92, 'Margot', 'Robbie', 'ausztrál', '1990.07.02', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (93, 'Chris', 'Pine', 'amerikai', '1980.08.26', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (94, 'Jason', 'Momoa', 'hawaii', '1979.08.01', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (95, 'Ezra', 'Miller', 'amerikai', '1992.09.30', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (96, 'Amber', 'Heard', 'amerikai', '1986.04.22', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (97, 'Willem', 'Dafoe', 'amerikai', '1955.07.22', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (98, 'Patrick', 'Wilson', 'amerikai', '1973.07.03', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (99, 'Nicole', 'Kidman', 'ausztrál', '1967.06.20', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (100, 'Zachary', 'Levi', 'amerikai', '1980.09.29', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (101, 'Mark', 'Strong', 'angol', '1963.08.05', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (102, 'Asher', 'Angel', 'amerikai', '2002.09.06', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (103, 'Christian', 'Bale', 'walesi', '1974.01.30', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (104, 'Michael', 'Caine', 'brit', '1933.03.14', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (105, 'Ken', 'Watanabe', 'japán', '1959.10.21', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (106, 'Liam', 'Neeson', 'ír', '1952.06.07', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (107, 'Katie', 'Holmes', 'amerikai', '1978.12.18', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (108, 'Gary', 'Oldman', 'angol', '1958.03.21', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (109, 'Cillian', 'Murphy', 'ír', '1976.05.25', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (110, 'Heath', 'Ledger', 'ausztrál', '1979.04.04', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (111, 'Aaron', 'Eckhart', 'amerikai', '1968.03.12', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (112, 'Maggie', 'Gyllenhaal', 'amerikai', '1977.11.16', 'kék');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (113, 'Tom', 'Hardy', 'angol', '1977.09.15', 'zöld');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (114, 'Anne', 'Hathaway', 'amerikai', '1982.11.12', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (115, 'Joseph', 'Gordon-Levitt', 'amerikai', '1981.02.17', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (116, 'Ryan', 'Reynolds', 'kanadai', '1976.10.23', 'barna');
INSERT INTO szinesz(szineszid,keresztnev, vezeteknev, nemzetiseg, sz_datum, szemszin) VALUES (117, 'Blake', 'Lively', 'amerikai', '1987.08.25', 'kék');

INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (1, 'Jon','Favreu', 'amerikai', '1966.10.19', 500000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (2, 'Louis','Leterrier', 'francia', '1973.06.17',65000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (3, 'Kenneth','Branagh', 'brit', '1982.04.05', 440000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (4, 'Joe','Johnston', 'amerikai', '1950.05.13', 768000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (5, 'Joss','Whedon', 'amerikai', '1964.06.23', 345400);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (6, 'Shane','Black', 'amerikai', '1961.12.16', 1000000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (7, 'Alan','Taylor', 'amerikai', '1965.03.02', 570200);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (8, 'Anthony','Russo', 'amerikai', '1970.02.03', 1000000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (9, 'James','Gunn', 'amerikai', '1966.08.05', 987000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (10, 'Edgar','Wright', 'angol', '1974.04.18', 567300);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (11, 'Scott','Derrickson', 'amerikai', '1966.07.16', 890000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (12, 'Jon','Watts', 'amerikai', '1981.06.28', 650000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (13, 'Taika','Waititi', 'új-zélandi', '1975.08.16', 900000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (14, 'Ryan','Coogler', 'amerikai', '1986.05.23', 560000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (15, 'Peyton','Reed', 'amerikai', '1964.07.23', 638000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (16, 'Anna','Boden', 'amerikai', '1976.09.20', 120000);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (17, 'Cate','Shortland', 'ausztrál', '1968.08.10', 897645);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (18, 'Chloé','Zao', 'kínai', '1982.03.31', 563920);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (19, 'Zack','Snyder', 'amerikai', '1966.03.01', 987654);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (20, 'David','Ayer', 'amerikai', '1968.01.18', 123456);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (21, 'Patty','Jenkins', 'amerikai', '1971.07.24', 456345);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (22, 'James','Wan', 'ausztrál', '1977.02.26', 456234);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (23, 'David F.','Sandberg', 'svéd', '1981.01.21', 345789);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (24, 'Christopher','Nolan', 'angol', '1970.07.30', 2323454);
INSERT INTO rendezo(rendezoid, keresztnev, vezeteknev, nemzetiseg, sz_datum, fizetes) VALUES (25, 'Martin','Campbell', 'új-zélandi', '1943.10.24', 456666);


INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(1,'A Vasember', '2008.05.02', 126, 'Tony Stark, a zseniális feltaláló és különc milliárdos éppen legújabb szuperfegyverét mutatja be, amikor a csoportot támadás éri és Tony mellkasába vasszilánk fúródik, mely lassan halad a szíve felé.', 2, 1, 585000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(2,'A hihetetlen Hulk', '2008.06.13',112, 'Bruce Banner, a tudós kétségbeesetten keresi a gyógymódot arra a gamma-sugárzásra, amely megmérgezte a sejtjeit, és egy fékezhetetlen haragú erőt szabadít fel benne: a Hulkot.', 2, 2,264000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(3, 'Vasember 2.', '2010.05.07',124, 'Kiderült, hogy a szuperhős Vasember valójában Tony Stark. Kihasználva az iránta való érdeklődést a milliárdos feltaláló fel akarja hívni a figyelmet a közjó szolgálatába állítható technikai újdonságokra.', 2, 1,624000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(4, 'Thor','2011.04.21', 115, 'A hatalmas Thor rettenthetetlen harcos, óriási pörölyéről legendákat mesélnek. A csatában vitéz és legyőzhetetlen, ám makacssága és hirtelen haragú természete nem teszi őt alkalmassá arra, hogy apját kövesse Ashgar trónján.', 2, 3, 450000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(5,'Amerika Kapitány: Az első bosszúálló', '2011.07.22', 124, 'Javában dúl a második világháború 1941-ben. A lelkes és mindenre elszánt ifjú, Steve Rogers is jelentkezik a hadseregbe, ám a gyenge fizikuma miatt kiszuperálják a sorozáson.', 2, 4, 371000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(6, 'Bosszúállók', '2012.05.04',142, 'Nick Fury (Samuel L. Jackson), az Ügynökség vezetője új fenyegetést észlel. Az ellenfél minden eddiginél nagyobb veszélyt jelent a Földre. Fury a katasztrófa elkerülésére összehívja a szuperhősöket.', 2, 5, 624000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(7, 'Vasember 3.', '2013.05.03', 109, 'A legutóbbi küldetése után Tony Stark talonba tette a Vasembert. A tudós-szuperhős boldogan él egykori asszisztensével, ám álmatlanságban szenved, és pánikrohamok gyötrik.', 2, 6, 12150000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(8, 'Thor: Sötét világ', '2013.11.08', 120, '
Amikor a Föld és a Kilenc Birodalom világa fedésbe kerül, megnyílnak a bolygók közötti dimenziókapuk. Ezt kihasználva akar mindent elpusztítani Malekith, a sötét elf, hogy visszataszítsa az univerzumot a teremtés előtti sötétségbe.', 2, 7, 644000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(9, 'Amerika Kapitány: A tél katonája', '2014.04.04', 136, 'Két évvel a New Yorkért folytatott csata után Amerika kapitány, azaz Steve Rogers Washingtonban él, és próbál alkalmazkodni a modern világ kihívásaihoz. Amikor azonban a S.H.I.E.L.D. egyik munkatársát megtámadják, a világot fenyegető összeesküvés közepén találja magát.', 2, 8, 714000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(10, 'A galaxis őrzői', '2014.08.01',122, ' kalandor Peter Quill ellopja a titokzatos gömböt, amelyre a bolygóközi uralomra törő Ronan is pályázik. A főgonosz ezért hajtóvadászatot indít utána. Menekülés közben Quill kényszerű szövetséget köt négy különös alakkal: Rockettel, Groottal, Gamorával és Draxszal.', 2, 9, 403000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(11, 'Bosszúállók: Ultron kora' , '2015.05.01', 142, 'Amikor Tony Stark újra életet akar lehelni a parkolópályára állított békefenntartó programba, nem minden sikerül a tervei szerint.', 2, 5, 14050000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(12, 'A Hangya', '2015.07.17', 118, 'A Marvel Moziuniverzum következő epizódjában a nézők megismerkedhetnek az eredeti Bosszúállók egyik alapítójával, aki első alkalommal látható a mozivásznon. A Hangya szuperképessége, hogy rendkívül kis méretűre tud zsugorodni, miközben ereje megsokszorozódik.', 2, 10, 180000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(13, 'Amerika Kapitány: Polgárháború', '2016.05.06', 148, 'Az Amerika kapitány: Polgárháborúban Steve Rogers az újjáalakult Bosszúállók csapatának élén próbál gondoskodni az emberiség biztonságáról. Azonban egy újabb incidens következik be, amelynek nem várt következményei lesznek.', 2, 8, 11530000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(14, 'Doctor Strange', '2016.11.06', 115, 'A világhírű idegsebész, Dr. Stephen Strange élete tragikus fordulatot vesz egy szörnyű autóbaleset következtében.', 2, 11, 677000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(15, 'A galaxis őrzői vol. 2.', '2017.05.05', 136, 'Az őrzőknek sokat kell tenniük azért, hogy összetartsák újdonsült családjukat, miközben kiderítenek egyet s mást Peter Quill igazi szüleiről is.', 2, 9, 8638000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(16, 'Pókember: Hazatérés', '2017.07.07', 133, 'Pókember most egészen új ellenségekkel találkozik, és úgy tűnik, még a szokásos humora is kevés lehet hozzá, hogy életben maradjon. Az új bajban új szövetségesek után kell néznie.', 2, 12, 880000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(17, 'Thor: Ragnarök', '2017.11.03', 131, 'A Marvel Studios új filmjében Thort pörölyétől megfosztva bebörtönzik az univerzum túlsó felén.', 2, 13, 854000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(18, 'Fekete Párduc', '2018.02.16', 134, 'A Marvel Studios Fekete Párduc című új filmjének hőse T-Challa, aki apja halálát követően hazatér az elszigetelt, ám technológiailag fejlett afrikai országba, Wakanda királyságába, hogy elfoglalja az őt megillető helyet a trónon.', 2, 14, 1344000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(19, 'Bosszúállók: Végtelen háború', '2018.04.27', 149, 'A Marvel mozis univerzumának egészét felölelő, tíz év eseményeinek tetőpontját jelentő Bosszúállók: Végtelen háborúban minden idők legnagyobb és leghalálosabb leszámolására kerül sor.', 2, 8, 20480000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(20, 'A Hangya és a Darázs', '2018.07.06', 118, 'A Marvel Studios új filmjében az Amerika Kapitány: Polgárháború eseményeit követően Scott Lang kénytelen viselni döntései következményeit szuperhősként és apaként egyaránt.', 2, 15, 622000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(21, 'Marvel Kapitány', '2019.03.08', 124, 'Mikor egy két idegen faj közötti galaktikus háború eléri a Földet, Danvers néhány szövetségessel együtt az események forgatagában találja magát, s hamarosan a világegyetem egyik legerősebb hősévé válik.', 2, 16, 1128000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(22, 'Bosszúállók: Végjáték', '2019.04.26', 182, 'Thanos súlyos tette, amivel elpusztította az univerzum élőlényeinek felét és megtörte a Bosszúállókat, a megmaradt hősöket egy végső összecsapásra készteti a Marvel Studios huszonegy filmet megkoronázó, nagyszabású fináléjában, a Bosszúállók: Végjátékban.', 2, 8, 27960000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(23, 'Pókember: Idegenben', '2019.07.02', 129, 'A rejtélyes Mysterio (Jake Gyllenhaal) miatt új kalandba indul, és könnyen lehet, hogy Pókember, aki nagy nehezen próbálja a hátizsákos utazást a hősi küzdelemmel összehangolni, kénytelen régi Bosszúálló társaitól segítséget kérni', 2, 12, 11290000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(24, 'Fekete Özvegy', '2020.05.01', null, null, 2, 17, null);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(25, 'Eternals', '2020.11.06', null, null, 2, 18, null);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(26, 'Doctor Strange in the Multiverse of Madness','2021.05.07', null, null, 2, 11, null);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES(27, 'Thor: Love and Thunder','2021.11.05', null, null, 2, 13, null);

INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (28, 'Az acélember', '2013.06.10', 143, 'A kis Clark egy egyszerű farmer házaspár, Martha és Jonathan Kent gyermeke. Egy napon ráébred arra, hogy rendkívüli adottságokkal rendelkezik, és olyasmikre képes, amire egyik barátja, sőt egyetlen földi halandó sem.', 1, 19, 668000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (29, 'Batman Superman ellen – Az igazság hajnala','2016.05.25', 151, 'Milyen hősre van szüksége a világnak? Gotham City védelmezője úgy érzi, hogy Metropolis megmentője már istenként tekint magára, senki és semmi nem korlátozza, így szembefordul vele.', 1, 19, 872700000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (30, 'Suicide Squad – Öngyilkos osztag','2016.06.05', 130, 'Elkapták őket, és mindenre hajlandóak, hogy újra kiszabadulhassanak. Egy titkos állambiztonsági szervezet, az A.R.G.U.S. összefogdosta a gonosz szuperhősök leggonoszabbjait, és különleges börtönökben helyezte el őket: úgy, hogy semmiképpen ne szökhessenek meg.', 1, 20, 45600000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (31, 'Wonder Woman', '2017.06.02', 141, 'Nem hívták mindig Wonder Womannek. Valaha ő volt Diana (Gal Gadot), az amazonok hercegnője, aki egy távoli, titkos szigeten, csupa nő között élt. Uralkodónak nevelték, legyőzhetetlen harcosnak képezték ki, de békében teltek a napjai. Míg meg nem látta az első férfit.', 1, 21, 876000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (32, 'Az igazság ligája', '2017.11.17', 121, 'Bruce Wayne tudja, hogy nem vonulhat vissza. A világnak szüksége van rá, és a hozzá hasonlókra, mert különben védtelen. Új szövetségese, Diane Prince segítségével csapatot toboroz.', 1, 19, 675000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (33, 'Aquaman', '2018.12.21', 143, 'Ő az igazság ligája legerősebb tagja. Arthur Curry megtudja, hogy Atlantis, a víz alatti királyság trónja rá vár. Mint törvényes örökösnek, neki kell elfogadnia a koronát, hogy csatába vezesse a népét – mert a világ bajban van, és nincs, más, aki kiálljon érte.', 1, 22, 987000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (34, 'Shazam!', '2019.04.05', 132, 'Mindannyiunkban ott bujkál a szuperhős – de van, akiből elő is jön. Billy Batson 14 éves, egy árvaházból kerül nevelőszüleihez, és nem a legnépszerűbb srác az iskolában…de Shazam, az ősöreg, különleges erejű varázsló mégis kiválasztja.', 1, 23, 567000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (35, 'Batman: Kezdődik!', '2005.07.14', 140, 'Bruce Wayne (Christian Bale), az árva milliomos önszántából járja meg az erőszak poklát, amikor Henri Ducard (Liam Neeson), a keleti mester a harcosai közé veszi és tökéletesen kiképzi. Wayne azonban nem osztozik mestere elvakult nézeteiben és visszatér Gotham Citybe.', 1, 24, 375000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (36, 'A sötét lovag', '2008.08.07', 152, 'Batman (Christian Bale), Gordon felügyelő (Gary Oldman) és a megvesztegethetetlen ügyész, Harvey Dent (Aaron Eckhart) hatásos hadjáratot indítanak a bűnözők ellen. Önként vállalt feladatukat már-már siker koronázza, ám ekkor megjelenik Joker (Heath Ledger), aki a bűnözők között is rettegett, ravaszabb és veszélyesebb bárkinél.', 1, 24, 533000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (37, 'A sötét lovag – Felemelkedés', '2012.07.26', 165, 'yolc év telt el azóta, hogy egy szörnyű éjszakát követően Batman nyom nélkül eltűnt. Magára vállalta Harvey Dent államügyész minden vétkét, és a hősből üldözött bűnöző lett. Önfeláldozása azonban nem volt hiábavaló: a Dent-törvénynek köszönhetően Gotham békés várossá vált. Átmenetileg.', 1, 24, 456000000);
INSERT INTO film(filmid,cim, kijovetel, hossz, cselekmeny, univerzumid, rendezoid, bevetel) VALUES (38, 'Zöld Lámpás', '2011.08.11', 114, 'A Zöld Lámpás Alakulat tagjainak feladata a világűr békéjének őrzése. Különleges gyűrűt viselnek, mely különleges képességeket biztosít számukra. Nem elég, hogy az univerzum egyensúlyát minden eddiginél veszélyesebb ellenfél veszélyezteti, az Alakulat tagjai egy földi embert is kénytelenek bevenni a csapatba.', 1, 25, 345000000);

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (1, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (1, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (1, 8, 'James Rhodes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (1, 9, 'Obadiah Stane');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (1, 10, 'Happy Hogan');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (2, 11, 'Bruce Banner');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (2, 12, 'Betty Ross');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 10, 'Happy Hogan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 13, 'Ivan Vanko');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 16, 'Agent Coulson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 15, 'Natalie Rushman');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (3, 14, 'James Rhodes');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 18, 'Odin');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 19, 'Jane Foster');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 21, 'Erik Selvig');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 22, 'Darcy Lewis');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 16, 'Agent Coulson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (4, 23, 'Heimdall');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (5, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (5, 2, 'Johann Schmidt');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (5, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (5, 4, 'Peggy Carter');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (5, 5, 'Bucky Barnes');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 24, 'Clint Barton');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 25, 'Brucer Banner');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 16, 'Agent Coulson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 21, 'Erik Selvig');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (6, 7, 'Pepper Potts');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (7, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (7, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (7, 14, 'James Rhodes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (7, 10, 'Happy Hogan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (7, 28, 'Maya Hansen');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (7, 27, 'Aldrich Killian');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 18, 'Odin');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 19, 'Jane Foster');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 21, 'Erik Selvig');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 22, 'Darcy Lewis');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (8, 23, 'Heimdall');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 29, 'Sam Wilson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 5, 'Bucky Barnes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 30, 'Kate');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (9, 4, 'Peggy Carter');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 31, 'Peter Quill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 32, 'Groot');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 33, 'Rocket');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 34, 'Gamora');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 35, 'Drax');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 36, 'Ronan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 37, 'Yondu Udonta');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (10, 38, 'Nebula');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 24, 'Clint Barton');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 25, 'Brucer Banner');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 16, 'Agent Coulson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 21, 'Erik Selvig');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 4, 'Peggy Carter');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 23, 'Heimdall');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 39, 'Ultron');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 40, 'Pietro Maximoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 41, 'Wanda Maximoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 29, 'Sam Wilson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (11, 73, 'Thanos');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 42, 'Scott Lang');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 43, 'Dr. Hank Pym');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 44, 'Darren Cross');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 45, 'Hope van Dyne');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 29, 'Sam Wilson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 46, 'Maggie Lang');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (12, 4, 'Peggy Carter');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 24, 'Clint Barton');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 16, 'Agent Coulson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 21, 'Erik Selvig');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 42, 'Scott Lang');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 29, 'Sam Wilson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 41, 'Wanda Maximoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 5, 'Bucky Barnes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 30, 'Kate');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 14, 'James Rhodes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 47, 'T-Challa');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 70, 'Everett K. Ross');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 48, 'Vision');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 49, 'Peter Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (13, 50, 'May Parker');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (14, 51, 'Dr. Stephen Strange');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (14, 52, 'Mordo');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (14, 53, 'Dr. Christine Palmer');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (14, 54, 'Wong');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (14, 55, 'Kaecilius');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (14, 56, 'The Ancient One');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 31, 'Peter Quill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 32, 'Groot');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 33, 'Rocket');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 34, 'Gamora');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 35, 'Drax');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 36, 'Ronan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 37, 'Yondu Udonta');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 57, 'Mantis');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 58, 'Ego');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (15, 59, 'Ayesha');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 49, 'Peter Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 50, 'May Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 10, 'Happy Hogan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 60, 'Adrian Toomes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 61, 'Michelle');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 62, 'Ned');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (16, 63, 'Liz');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 18, 'Odin');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 23, 'Heimdall');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 64, 'Hela');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 65, 'Grandmaster');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 25, 'Brucer Banner');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 66, 'Valkűr');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (17, 67, 'Skurge');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (18, 47, 'T-Challa');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (18, 70, 'Everett K. Ross');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (18, 69, 'Okoye');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (18, 68, 'Nakia');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (18, 71, 'Erik Killmonger');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (18, 72, 'Shuri');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 25, 'Brucer Banner');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 14, 'James Rhodes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 51, 'Dr. Stephen Strange');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 49, 'Peter Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 47, 'T-Challa');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 34, 'Gamora');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 38, 'Nebula');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 48, 'Vision');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 41, 'Wanda Maximoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 29, 'Sam Wilson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 5, 'Bucky Barnes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 23, 'Heimdall');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 69, 'Okoye');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 54, 'Wong');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 57, 'Mantis');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 35, 'Drax');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 32, 'Groot');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 33, 'Rocket');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 31, 'Peter Quill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 73, 'Thanos');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (19, 72, 'Shuri');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (20, 42, 'Scott Lang');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (20, 45, 'Hope van Dyne');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (20, 46, 'Maggie Lang');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (21, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (21, 74, 'Carol Danvers');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (21, 36, 'Ronan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (21, 16, 'Agent Coulson');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 3, 'Nick Fury');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 6, 'Tony Stark');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (2, 25, 'Brucer Banner');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 1, 'Amerika Kapitány');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 14, 'James Rhodes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 51, 'Dr. Stephen Strange');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 49, 'Peter Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 47, 'T-Challa');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 34, 'Gamora');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 38, 'Nebula');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 20, 'Loki');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 48, 'Vision');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 41, 'Wanda Maximoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 29, 'Sam Wilson');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 5, 'Bucky Barnes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 23, 'Heimdall');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 69, 'Okoye');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 54, 'Wong');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 57, 'Mantis');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 62, 'Ned');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 35, 'Drax');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 32, 'Groot');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 7, 'Pepper Potts');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 56, 'The Ancient One');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 19, 'Jane Foster');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 4, 'Peggy Carter');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 33, 'Rocket');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 31, 'Peter Quill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 43, 'Dr. Hank Pym');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 10, 'Happy Hogan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 66, 'Valkűr');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 73, 'Thanos');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 72, 'Shuri');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 74, 'Carol Danvers');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 50, 'May Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (22, 45, 'Hope van Dyne');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 49, 'Peter Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 50, 'May Parker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 10, 'Happy Hogan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 61, 'Michelle');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 26, 'Agent Maria Hill');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 62, 'Ned');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (23, 75, 'Quentin Beck');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (24, 15, 'Natasha Romanoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (24, 6, 'Tony Stark');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (25, 76, 'Thena');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (25, 77, 'Ikaris');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (25, 78, 'Dane Whitman');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (25, 79, 'Ajak');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (26, 51, 'Dr. Stephen Strange');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (26, 41, 'Wanda Maximoff');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (26, 54, 'Wong');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (27, 19, 'Jane Foster');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (27, 17, 'Thor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (27, 66, 'Valkűr');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (28, 80, 'Clark Kent');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (28, 81, 'Lois Lane');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (28, 82, 'General Zod');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (28, 83, 'Martha Kent');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 83, 'Martha Kent');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 80, 'Clark Kent');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 81, 'Lois Lane');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 84, 'Bruce Wayne');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 85, 'Lex Luthor');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 86, 'Perry White');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 87, 'Alfred');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (29, 88, 'Diana Prince');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (30, 90, 'Deadshot');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (30, 91, 'The Joker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (30, 92, 'Harley Quinn');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (31, 88, 'Diana Prince');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (31, 93, 'Steve Trevor');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 88, 'Diana Prince');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 94, 'Aquaman');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 95, 'The Flash');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 80, 'Clark Kent');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 87, 'Alfred');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 83, 'Martha Kent');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 81, 'Lois Lane');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (32, 84, 'Bruce Wayne');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (33, 94, 'Aquaman');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (33, 96, 'Mera');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (33, 97, 'Vulko');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (33, 98, 'Orm király');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (33, 99, 'Atlanna');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (34, 100, 'Shazam');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (34, 101, 'Dr. Sivana');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (34, 102, 'Billy Batson');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 103, 'Bruce Wayne');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 104, 'Alfred');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 105, 'Ra-s Al Ghul');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 106, 'Ducard');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 107, 'Rachel Dawes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 108, 'Jim Gordon');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (35, 109, 'Dr. Jonathan Crane');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 103, 'Bruce Wayne');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 110, 'Joker');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 111, 'Harvey Dent');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 104, 'Alfred');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 106, 'Ducard');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 112, 'Rachel Dawes');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 108, 'Jim Gordon');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (36, 109, 'Scarecrow');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (37, 103, 'Bruce Wayne');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (37, 104, 'Alfred');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (37, 108, 'Jim Gordon');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (37, 113, 'Bane');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (37, 114, 'Selina');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (37, 115, 'Blake');

INSERT INTO szereples(filmid, szineszid, szerep) VALUES (38, 116, 'Hal Jordan');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (38, 117, 'Carol Ferris');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (38, 101, 'Sinestro');
INSERT INTO szereples(filmid, szineszid, szerep) VALUES (38, 89, 'Carl Ferris');

