﻿// <copyright file="DirectorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Director repo.
    /// </summary>
    public class DirectorRepository : IDirectorRepository
    {
        private readonly ComicsMoviesDatabaseEntities db;
        private List<string> names = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectorRepository"/> class.
        /// The constructor.
        /// </summary>
        public DirectorRepository()
        {
            this.db = new ComicsMoviesDatabaseEntities();
        }

        /// <summary>
        /// Delete a director with the given name.
        /// </summary>
        /// <param name="name">director's name.</param>
        public void DeleteDirector(string name)
        {
            if (this.IsInDatabase(name))
            {
                var movies = from x in this.db.rendezo
                             join y in this.db.film
                             on x.rendezoid equals y.rendezoid
                             where x.keresztnev + " " + x.vezeteknev == name
                             select y.filmid;

                foreach (var item in movies)
                {
                    var vmi = this.db.film.Single(x => x.filmid == item);
                    vmi.rendezoid = null;
                }

                var delete = this.db.rendezo.Single(x => x.keresztnev + " " + x.vezeteknev == name);
                this.db.rendezo.Remove(delete);
                this.db.SaveChanges();
            }
            else
            {
                throw new GivenEntityNotInDatabaseException("This Director is not in the database");
            }
        }

        /// <summary>
        /// Lists all of the director entities.
        /// </summary>
        /// <returns>All of directors.</returns>
        public List<rendezo> GetAll()
        {
            return this.db.rendezo.ToList();
        }

        /// <summary>
        /// Lists director names.
        /// </summary>
        /// <returns>list of director names.</returns>
        public List<string> ListDirectorsWithNames()
        {
            List<string> directorNames = new List<string>();
            foreach (var item in this.db.rendezo)
            {
                directorNames.Add(item.keresztnev + " " + item.vezeteknev);
            }

            return directorNames;
        }

        /// <summary>
        /// Updates the director's salary.
        /// </summary>
        /// <param name="name">director name.</param>
        /// <param name="newSalary">newSalary.</param>
        public void UpdateDirectorSalary(string name, string newSalary)
        {
            if (this.IsInDatabase(name))
            {
                var director = this.db.rendezo.Single(x => x.keresztnev + " " + x.vezeteknev == name);
                director.fizetes = int.Parse(newSalary);
                this.db.SaveChanges();
            }
            else
            {
                throw new Exception("The Director is not in the database!");
            }
        }

        /// <summary>
        /// Add a new director to the database.
        /// </summary>
        /// <param name="entity">the director entity.</param>
        public void Add(rendezo entity)
        {
            var temp = from x in this.db.rendezo
                       orderby x.rendezoid descending
                       select new
                       {
                           x.rendezoid,
                       };

            var lastID = temp.First().rendezoid;
            entity.rendezoid = lastID + 1;

            rendezo director = new rendezo()
            {
                keresztnev = entity.keresztnev,
                vezeteknev = entity.vezeteknev,
                fizetes = entity.fizetes,
                nemzetiseg = entity.nemzetiseg,
                sz_datum = entity.sz_datum,
            };
            this.names.Add(entity.keresztnev + " " + entity.vezeteknev);
            this.db.rendezo.Add(director);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Check if the director already in the database or not.
        /// </summary>
        /// <param name="name">the name of the circuit.</param>
        /// <returns>returns true if the director is in the database, false if not.</returns>
        public bool IsInDatabase(string name)
        {
            foreach (var item in this.db.rendezo)
            {
                this.names.Add(name);
            }

            return this.names.Contains(name);
        }

        /// <summary>
        /// Gives back the director with the specified id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a director.</returns>
        public rendezo GetOne(int id)
        {
            return this.db.rendezo.Where(x => x.rendezoid == id).FirstOrDefault();
        }
    }
}
