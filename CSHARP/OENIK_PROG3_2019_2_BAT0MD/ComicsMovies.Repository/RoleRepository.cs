﻿// <copyright file="RoleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Role repo.
    /// </summary>
    public class RoleRepository : IRoleRepository
    {
        private readonly ComicsMoviesDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRepository"/> class.
        /// The constructor.
        /// </summary>
        public RoleRepository()
        {
            this.db = new ComicsMoviesDatabaseEntities();
        }

        /// <summary>
        /// Add new role to the database.
        /// </summary>
        /// <param name="entity">role entity.</param>
        public void Add(szereples entity)
        {
            szereples newRole = new szereples()
            {
                filmid = entity.filmid,
                szineszid = entity.szineszid,
                szerep = entity.szerep,
            };
            this.db.szereples.Add(newRole);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Delete a role.
        /// </summary>
        /// <param name="title">title.</param>
        /// <param name="actorname">actorname.</param>
        public void DeleteRole(string title, string actorname)
        {
            var movie_id = (from x in this.db.film
                            where x.cim == title
                            select x.filmid).FirstOrDefault();

            var actor_id = (from y in this.db.szinesz
                            where y.keresztnev + " " + y.vezeteknev == actorname
                            select y.szineszid).FirstOrDefault();

            var delete = this.db.szereples.Where(x => x.szineszid == actor_id && x.filmid == movie_id).FirstOrDefault();

            this.db.szereples.Remove(delete);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Lists all of the role entities.
        /// </summary>
        /// <returns>All of the role entities.</returns>
        public List<szereples> GetAll()
        {
            return this.db.szereples.ToList();
        }

        /// <summary>
        /// Gives back the role with the specified id-s.
        /// </summary>
        /// <param name="movieid">movieid.</param>
        /// <param name="actorid">actorid.</param>
        /// <returns>a role.</returns>
        public szereples GetOne(int movieid, int actorid)
        {
            return this.db.szereples.Where(x => x.filmid == movieid && x.szineszid == actorid).FirstOrDefault();
        }

        /// <summary>
        /// Lists the actor's roles in movies.
        /// </summary>
        /// <returns>list of roles.</returns>
        public List<string> ListRoleEntitiesWithNames()
        {
            List<string> role_s = new List<string>();

            var roles = from actor in this.db.szinesz
                        join role in this.db.szereples
                        on actor.szineszid equals role.szineszid
                        join movie in this.db.film
                        on role.filmid equals movie.filmid
                        select new
                        {
                            Name = actor.keresztnev + " " + actor.vezeteknev,
                            MovieTitle = movie.cim,
                            Role = role.szerep,
                        };
            foreach (var role in roles)
            {
                role_s.Add(role.Name);
                role_s.Add(role.MovieTitle);
                role_s.Add(role.Role);
            }

            return role_s;
        }
    }
}
