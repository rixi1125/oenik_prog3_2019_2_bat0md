﻿// <copyright file="GivenEntityNotInDatabaseException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Exception.
    /// </summary>
    public class GivenEntityNotInDatabaseException : Exception
    {
        /// <summary>
        /// Unique Message of the Exception.
        /// </summary>
        private string uniqueMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="GivenEntityNotInDatabaseException"/> class.
        /// The constructor of the Exception.
        /// </summary>
        /// <param name="message">the message of the exception.</param>
        public GivenEntityNotInDatabaseException(string message)
        {
            this.uniqueMessage = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GivenEntityNotInDatabaseException"/> class.
        /// </summary>
        /// <param name="info">name.</param>
        /// <param name="context">idk.</param>
        protected GivenEntityNotInDatabaseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
