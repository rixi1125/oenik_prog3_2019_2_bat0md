﻿// <copyright file="MovieRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Movie repo.
    /// </summary>
    public class MovieRepository : IMovieRepository
    {
        private readonly ComicsMoviesDatabaseEntities db;
        private List<string> names = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieRepository"/> class.
        /// The constructor.
        /// </summary>
        public MovieRepository()
        {
            this.db = new ComicsMoviesDatabaseEntities();
        }

        /// <summary>
        /// Deletes a movie with the given name.
        /// </summary>
        /// <param name="name">movie title.</param>
        public void DeleteMovie(string name)
        {
            if (this.IsInDatabase(name))
            {
                var roles = from x in this.db.film
                            join y in this.db.szereples
                            on x.filmid equals y.filmid
                            where x.cim == name
                            select y;

                foreach (var item in roles)
                {
                    this.db.szereples.Remove(item);
                }

                var delete = this.db.film.Single(x => x.cim == name);
                this.db.film.Remove(delete);
                this.db.SaveChanges();
            }
            else
            {
                throw new GivenEntityNotInDatabaseException("This Movie is not in the database");
            }
        }

        /// <summary>
        /// Lists all of the movie entities.
        /// </summary>
        /// <returns>All of the movie entities.</returns>
        public List<film> GetAll()
        {
            return this.db.film.ToList();
        }

        /// <summary>
        /// Updates a movie's title.
        /// </summary>
        /// <param name="oldTitle">oldTitle.</param>
        /// <param name="newTitle">newTitle.</param>
        public void UpdateMovieTitle(string oldTitle, string newTitle)
        {
            if (this.IsInDatabase(oldTitle))
            {
                var movie = this.db.film.Single(x => x.cim == oldTitle);
                movie.cim = newTitle;
                this.db.SaveChanges();
            }
            else
            {
                throw new Exception("The Movie is not in the database!");
            }
        }

        /// <summary>
        /// Add a new movie to the database.
        /// </summary>
        /// <param name="entity">the movie entity.</param>
        public void Add(film entity)
        {
            var temp = from x in this.db.film
                       orderby x.filmid descending
                       select new
                       {
                           x.filmid,
                       };

            var lastID = temp.First().filmid;
            entity.filmid = lastID + 1;

            film movie = new film()
            {
                kijovetel = entity.kijovetel,
                bevetel = entity.bevetel,
                cim = entity.cim,
                cselekmeny = entity.cselekmeny,
                hossz = entity.hossz,
                rendezoid = entity.rendezoid,
                univerzumid = entity.univerzumid,
            };
            this.names.Add(entity.cim);
            this.db.film.Add(movie);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Check if the movie already in the database or not.
        /// </summary>
        /// <param name="name">the name of the circuit.</param>
        /// <returns>returns true if the movie is in the database, false if not.</returns>
        public bool IsInDatabase(string name)
        {
            foreach (var item in this.db.film)
            {
                this.names.Add(item.cim);
            }

            return this.names.Contains(name);
        }

        /// <summary>
        /// Lists movies with titles.
        /// </summary>
        /// <returns>list of movie titles.</returns>
        public List<string> ListMoviesWithTitle()
        {
            List<string> movies = new List<string>();

            foreach (var item in this.db.film)
            {
                movies.Add(item.cim);
            }

            return movies;
        }

        /// <summary>
        /// Gives back the movie with the specified id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a movie entity.</returns>
        public film GetOne(int id)
        {
            return this.db.film.Where(x => x.filmid == id).FirstOrDefault();
        }
    }
}
