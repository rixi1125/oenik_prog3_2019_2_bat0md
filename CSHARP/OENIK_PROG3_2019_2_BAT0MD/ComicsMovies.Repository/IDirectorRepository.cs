﻿// <copyright file="IDirectorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Interface for directors.
    /// </summary>
    public interface IDirectorRepository : IRepository<rendezo>
    {
        /// <summary>
        /// Delete method.
        /// </summary>
        /// <param name="name">the director's name.</param>
        void DeleteDirector(string name);

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="name">director name.</param>
        /// <param name="newSalary">newSalary.</param>
        void UpdateDirectorSalary(string name, string newSalary);

        /// <summary>
        /// GetOne method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>an director.</returns>
        rendezo GetOne(int id);

        /// <summary>
        /// ListDirectorsWithNames method.
        /// </summary>
        /// <returns>string list of director names.</returns>
        List<string> ListDirectorsWithNames();
    }
}
