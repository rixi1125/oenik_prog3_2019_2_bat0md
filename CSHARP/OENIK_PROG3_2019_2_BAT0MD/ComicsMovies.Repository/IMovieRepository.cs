﻿// <copyright file="IMovieRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Interface for movies.
    /// </summary>
    public interface IMovieRepository : IRepository<film>
    {
        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="oldTitle">oldTitle.</param>
        /// <param name="newTitle">newTitle.</param>
        void UpdateMovieTitle(string oldTitle, string newTitle);

        /// <summary>
        /// Delete method.
        /// </summary>
        /// <param name="name">movie title.</param>
        void DeleteMovie(string name);

        /// <summary>
        /// GetOne method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a movie.</returns>
        film GetOne(int id);

        /// <summary>
        /// ListMoviesWithTitle method.
        /// </summary>
        /// <returns>string list of movie titles.</returns>
        List<string> ListMoviesWithTitle();
    }
}
