﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;
    using ComicsMovies.Logic;
    using ComicsMovies.Repository;
    using Moq;
    using NUnit;
    using NUnit.Framework;

    /// <summary>
    /// Making tests.
    /// </summary>
    [TestFixture]
    internal class Tests
    {
        private static Mock<IActorRepository> mockedActorrepo = new Mock<IActorRepository>();

        private static Mock<IMovieRepository> mockedMovierepo = new Mock<IMovieRepository>();

        private static Mock<IRoleRepository> mockedRolerepo = new Mock<IRoleRepository>();

        private static Mock<IDirectorRepository> mockedDirectorrepo = new Mock<IDirectorRepository>();

        private ComicsLogic comicsLogic = new ComicsLogic(mockedMovierepo.Object, mockedDirectorrepo.Object, mockedActorrepo.Object, mockedRolerepo.Object);

        /// <summary>
        /// Tests the Getall methods.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            List<szinesz> szineszek = new List<szinesz>();
            szineszek.Add(new szinesz() { szineszid = 1, keresztnev = "Chris", vezeteknev = "Evans", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = new DateTime(1992, 11, 12) });
            szineszek.Add(new szinesz() { szineszid = 6, keresztnev = "Robert", vezeteknev = "Downey Jr.", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = new DateTime(1970, 11, 11) });

            mockedActorrepo.Setup(rep => rep.GetAll()).Returns(szineszek);
            Assert.That(this.comicsLogic.ListActors().Skip(1).First().keresztnev.Equals("Robert"));
        }

        /// <summary>
        /// Tests the GetAll method in every repo.
        /// </summary>
        [Test]
        public void ListAllTest()
        {
            List<szinesz> actorList = new List<szinesz>()
            {
               new szinesz() { szineszid = 1, keresztnev = "Chris", vezeteknev = "Evans", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1981-06-13", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 6, keresztnev = "Robert", vezeteknev = "Downey Jr.", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = DateTime.ParseExact("1965-04-04", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 7, keresztnev = "Gwyneth", vezeteknev = "Paltrow", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1972-09-27", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 15, keresztnev = "Scarlett", vezeteknev = "Johansson", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1984-11-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 17, keresztnev = "Chris", vezeteknev = "Hemsworth", nemzetiseg = "ausztrál", szemszin = "kék", sz_datum = DateTime.ParseExact("1983-08-11", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 20, keresztnev = "Tom", vezeteknev = "Hiddleston", nemzetiseg = "brit", szemszin = "kék", sz_datum = DateTime.ParseExact("1981-02-29", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 25, keresztnev = "Mark", vezeteknev = "Ruffalo", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = DateTime.ParseExact("1967-11-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 117, keresztnev = "Blake", vezeteknev = "Lively", nemzetiseg = "amerikai", szemszin = "kék", sz_datum = DateTime.ParseExact("1987-08-25", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 42, keresztnev = "Paul", vezeteknev = "Rudd", nemzetiseg = "amerikai", szemszin = "kék", sz_datum = DateTime.ParseExact("1969-04-06", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 49, keresztnev = "Tom", vezeteknev = "Holland", nemzetiseg = "brit", szemszin = "barna", sz_datum = DateTime.ParseExact("1996-06-01", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
            };
            mockedActorrepo.Setup(x => x.GetAll()).Returns(actorList);
            Assert.That(() => mockedActorrepo.Object.GetAll().Count(), Is.EqualTo(10));
            Assert.That(() => mockedActorrepo.Object.GetAll().Count(), Is.Not.EqualTo(15));
            Assert.That(this.comicsLogic.ListActors().Skip(1).First().nemzetiseg.Equals("amerikai"));

            List<rendezo> directorList = new List<rendezo>()
            {
                new rendezo() { rendezoid = 1, keresztnev = "Jon", vezeteknev = "Favreu", nemzetiseg = "amerikai", fizetes = 500000, sz_datum = DateTime.ParseExact("1966-10-19", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
                new rendezo() { rendezoid = 4, keresztnev = "Joe", vezeteknev = "Johnston", nemzetiseg = "amerikai", fizetes = 768000, sz_datum = DateTime.ParseExact("1950-05-13", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
                new rendezo() { rendezoid = 24, keresztnev = "Christopher", vezeteknev = "Nolan", nemzetiseg = "angol", fizetes = 2323454, sz_datum = DateTime.ParseExact("1970-07-30", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
                new rendezo() { rendezoid = 25, keresztnev = "Martin", vezeteknev = "Campbell", nemzetiseg = "új-zélandi", fizetes = 456666, sz_datum = DateTime.ParseExact("1943-10-24", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
            };

            mockedDirectorrepo.Setup(x => x.GetAll()).Returns(directorList);
            Assert.That(() => mockedDirectorrepo.Object.GetAll().Count(), Is.Not.EqualTo(5));

            List<film> movieList = new List<film>()
            {
                new film() { filmid = 1, cim = "A Vasember", kijovetel = DateTime.ParseExact("2008-05-02", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 585000000, cselekmeny = "Tony Stark, a zseniális feltaláló és különc milliárdos éppen legújabb szuperfegyverét mutatja be.", hossz = 126, rendezoid = 1, univerzumid = 2 },
                new film() { filmid = 5, cim = "Amerika Kapitány: Az első bosszúálló", kijovetel = DateTime.ParseExact("2011-07-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 371000000, cselekmeny = "Javában dúl a második világháború 1941-ben.", hossz = 124, rendezoid = 4, univerzumid = 2 },
                new film() { filmid = 38, cim = "Zöld Lámpás", kijovetel = DateTime.ParseExact("2011-08-11", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 345000000, cselekmeny = "A Zöld Lámpás Alakulat tagjainak feladata a világűr békéjének őrzése.", hossz = 114, rendezoid = 25, univerzumid = 1 },
                new film() { filmid = 36, cim = "A sötét lovag", kijovetel = DateTime.ParseExact("2008-08-07", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 345000000, cselekmeny = "Batman (Christian Bale), Gordon felügyelő (Gary Oldman) és a megvesztegethetetlen ügyész, Harvey Dent (Aaron Eckhart) hatásos hadjáratot indítanak a bűnözők ellen.", hossz = 152, rendezoid = 24, univerzumid = 1 },
            };

            mockedMovierepo.Setup(x => x.GetAll()).Returns(movieList);
            Assert.That(() => mockedMovierepo.Object.GetAll().Count(), Is.EqualTo(4));

            List<szereples> roleList = new List<szereples>()
            {
                new szereples() { filmid = 1, szineszid = 6, szerep = "Tony Stark" },
                new szereples() { filmid = 1, szineszid = 7, szerep = "Pepper Potts" },
                new szereples() { filmid = 4, szineszid = 17, szerep = "Thor" },
                new szereples() { filmid = 5, szineszid = 1, szerep = "Amerika Kapitány" },
                new szereples() { filmid = 5, szineszid = 3, szerep = "Nick Fury" },
            };

            mockedRolerepo.Setup(x => x.GetAll()).Returns(roleList);
            Assert.That(() => mockedRolerepo.Object.GetAll(), Is.Not.EqualTo(4));
        }

        /// <summary>
        /// Tests the Update methods.
        /// </summary>
        [Test]
        public void UpdateTests()
        {
            rendezo dir = new rendezo() { rendezoid = 1, keresztnev = "Jon", vezeteknev = "Favreu", nemzetiseg = "amerikai", fizetes = 500000, sz_datum = DateTime.ParseExact("1966-10-19", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) };
            this.comicsLogic.UpdateDirectorSalary("Jon Favreu", "123");
            mockedDirectorrepo.Verify(rep => rep.UpdateDirectorSalary("Jon Favreu", "123"), Times.Exactly(1));
            Assert.That(() => this.comicsLogic.UpdateDirectorSalary("David Ayer", "23456"), Throws.Nothing);
            mockedDirectorrepo.Verify(rep => rep.UpdateDirectorSalary("David Ayer", "23456"), Times.Exactly(1));
            Assert.That(() => this.comicsLogic.UpdateActorLastName("Chris Evans", "Cap"), Throws.Nothing);
            mockedActorrepo.Verify(rep => rep.UpdateActorLastName("Chris Evans", "Cap"), Times.Exactly(1));
            Assert.That(() => this.comicsLogic.UpdateMovieTitle("Zöld Lámpás", "Szar film"), Throws.Nothing);
            mockedMovierepo.Verify(rep => rep.UpdateMovieTitle("Zöld Lámpás", "Szar film"), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the Delete methods.
        /// </summary>
        [Test]
        public void DeleteTests()
        {
            Assert.That(() => this.comicsLogic.DeleteActor("Scarlett Johansson"), Throws.Nothing);
            mockedActorrepo.Verify(x => x.DeleteActor("Scarlett Johansson"), Times.Exactly(1));
            Assert.That(() => this.comicsLogic.DeleteMovie("Bosszúállók"), Throws.Nothing);
            mockedMovierepo.Verify(x => x.DeleteMovie("Bosszúállók"), Times.Exactly(1));
            Assert.That(() => this.comicsLogic.DeleteDirector("Jon Favreu"), Throws.Nothing);
            mockedDirectorrepo.Verify(x => x.DeleteDirector("Jon Favreu"), Times.Exactly(1));
            Assert.That(() => this.comicsLogic.DeleteRole("Vasember 2.", "Gwyneth Paltrow"), Throws.Nothing);
            Assert.That(() => this.comicsLogic.DeleteRole("Vasember 2.", "Robert Downey Jr."), Throws.Nothing);
            mockedRolerepo.Verify(x => x.DeleteRole("Vasember 2.", "Robert Downey Jr."), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the Add methods.
        /// </summary>
        [Test]
        public void AddTests()
        {
            List<szinesz> szineszek = new List<szinesz>();
            mockedActorrepo.Setup(x => x.GetAll()).Returns(szineszek);
            szinesz sz = new szinesz() { szineszid = 1, keresztnev = "Chris", vezeteknev = "Evans", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = new DateTime(1992, 11, 11) };
            this.comicsLogic.AddActor("Chris", "Evan", "magyar", "1992.11.11", "zöld");
            mockedActorrepo.Verify(x => x.Add(sz), Times.Exactly(0));
        }

        /// <summary>
        /// Tests the Add methods.
        /// </summary>
        [Test]
        public void AddTests1()
        {
            Assert.That(() => this.comicsLogic.AddActor("Geza", "Kovacs", string.Empty, "1992.12.12", "zöld"), Throws.ArgumentException);
            Assert.That(() => this.comicsLogic.AddActor("Geza", "Kovacs", null, "1992.12.12", "zöld"), Throws.ArgumentNullException);
            Assert.That(() => this.comicsLogic.AddMovie("valami", "2010-11-12", null, "Kovács Géza elment aludni.", "Marvel", "Jon Favreu", "12345"), Throws.ArgumentNullException);
            Assert.That(() => this.comicsLogic.AddRole("Bosszúállók", null, "Wasp"), Throws.ArgumentNullException);
            Assert.Throws<FormatException>(() => this.comicsLogic.AddDirector("Tünde", "Kiszel", "magyar", "örökrefiatal", "320000000"));
            Assert.Throws<ArgumentNullException>(() => this.comicsLogic.AddDirector(null, null, "magyar", "1965-03-03", "320000000"));
            Assert.That(() => this.comicsLogic.AddDirector("Benny", "Cucumberbatch", "brit", "1965-03-03", "32000"), Throws.Nothing);
        }

        /// <summary>
        /// Tests the AverageIncomingGroupByUniverse non curd method.
        /// </summary>
        [Test]
        public void NonCrudTest1()
        {
            List<film> movieList = new List<film>()
            {
                new film() { filmid = 1, cim = "A Vasember", kijovetel = DateTime.ParseExact("2008-05-02", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 585000000, cselekmeny = "Tony Stark, a zseniális feltaláló és különc milliárdos éppen legújabb szuperfegyverét mutatja be.", hossz = 126, rendezoid = 1, univerzumid = 2 },
                new film() { filmid = 5, cim = "Amerika Kapitány: Az első bosszúálló", kijovetel = DateTime.ParseExact("2011-07-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 371000000, cselekmeny = "Javában dúl a második világháború 1941-ben.", hossz = 124, rendezoid = 4, univerzumid = 2 },
                new film() { filmid = 38, cim = "Zöld Lámpás", kijovetel = DateTime.ParseExact("2011-08-11", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 345000000, cselekmeny = "A Zöld Lámpás Alakulat tagjainak feladata a világűr békéjének őrzése.", hossz = 114, rendezoid = 25, univerzumid = 1 },
                new film() { filmid = 36, cim = "A sötét lovag", kijovetel = DateTime.ParseExact("2008-08-07", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 345000000, cselekmeny = "Batman (Christian Bale), Gordon felügyelő (Gary Oldman) és a megvesztegethetetlen ügyész, Harvey Dent (Aaron Eckhart) hatásos hadjáratot indítanak a bűnözők ellen.", hossz = 152, rendezoid = 24, univerzumid = 1 },
            };

            mockedMovierepo.Setup(x => x.GetAll()).Returns(movieList);
            Assert.That(this.comicsLogic.AverageIncomingGroupByUniverse().Skip(2).First().Equals("DC"));
        }

        /// <summary>
        /// Tests the ActorsInHowManyMovies non crud method.
        /// </summary>
        [Test]
        public void NonCurdTest2()
        {
            List<szinesz> actorList = new List<szinesz>()
            {
               new szinesz() { szineszid = 1, keresztnev = "Chris", vezeteknev = "Evans", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1981-06-13", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 6, keresztnev = "Robert", vezeteknev = "Downey Jr.", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = DateTime.ParseExact("1965-04-04", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 7, keresztnev = "Gwyneth", vezeteknev = "Paltrow", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1972-09-27", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 15, keresztnev = "Scarlett", vezeteknev = "Johansson", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1984-11-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 17, keresztnev = "Chris", vezeteknev = "Hemsworth", nemzetiseg = "ausztrál", szemszin = "kék", sz_datum = DateTime.ParseExact("1983-08-11", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 20, keresztnev = "Tom", vezeteknev = "Hiddleston", nemzetiseg = "brit", szemszin = "kék", sz_datum = DateTime.ParseExact("1981-02-29", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 25, keresztnev = "Mark", vezeteknev = "Ruffalo", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = DateTime.ParseExact("1967-11-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 117, keresztnev = "Blake", vezeteknev = "Lively", nemzetiseg = "amerikai", szemszin = "kék", sz_datum = DateTime.ParseExact("1987-08-25", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 42, keresztnev = "Paul", vezeteknev = "Rudd", nemzetiseg = "amerikai", szemszin = "kék", sz_datum = DateTime.ParseExact("1969-04-06", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 49, keresztnev = "Tom", vezeteknev = "Holland", nemzetiseg = "brit", szemszin = "barna", sz_datum = DateTime.ParseExact("1996-06-01", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
            };
            mockedActorrepo.Setup(x => x.GetAll()).Returns(actorList);

            List<szereples> roleList = new List<szereples>()
            {
                new szereples() { filmid = 1, szineszid = 6, szerep = "Tony Stark" },
                new szereples() { filmid = 1, szineszid = 7, szerep = "Pepper Potts" },
                new szereples() { filmid = 4, szineszid = 17, szerep = "Thor" },
                new szereples() { filmid = 5, szineszid = 1, szerep = "Amerika Kapitány" },
                new szereples() { filmid = 5, szineszid = 3, szerep = "Nick Fury" },
            };

            mockedRolerepo.Setup(x => x.GetAll()).Returns(roleList);
            Assert.That(this.comicsLogic.ActorsInHowManyMovies().First().Equals("Robert Downey Jr."));
            Assert.That(this.comicsLogic.ActorsInHowManyMovies().Last().Equals("1"));
        }

        /// <summary>
        /// Tests the GivenActorInHowManyMovies non crud method.
        /// </summary>
        [Test]
        public void NonCrudTest3()
        {
            List<szinesz> actorList = new List<szinesz>()
            {
               new szinesz() { szineszid = 1, keresztnev = "Chris", vezeteknev = "Evans", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1981-06-13", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 6, keresztnev = "Robert", vezeteknev = "Downey Jr.", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = DateTime.ParseExact("1965-04-04", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 7, keresztnev = "Gwyneth", vezeteknev = "Paltrow", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1972-09-27", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 15, keresztnev = "Scarlett", vezeteknev = "Johansson", nemzetiseg = "amerikai", szemszin = "zöld", sz_datum = DateTime.ParseExact("1984-11-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 17, keresztnev = "Chris", vezeteknev = "Hemsworth", nemzetiseg = "ausztrál", szemszin = "kék", sz_datum = DateTime.ParseExact("1983-08-11", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 20, keresztnev = "Tom", vezeteknev = "Hiddleston", nemzetiseg = "brit", szemszin = "kék", sz_datum = DateTime.ParseExact("1981-02-29", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 25, keresztnev = "Mark", vezeteknev = "Ruffalo", nemzetiseg = "amerikai", szemszin = "barna", sz_datum = DateTime.ParseExact("1967-11-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 117, keresztnev = "Blake", vezeteknev = "Lively", nemzetiseg = "amerikai", szemszin = "kék", sz_datum = DateTime.ParseExact("1987-08-25", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 42, keresztnev = "Paul", vezeteknev = "Rudd", nemzetiseg = "amerikai", szemszin = "kék", sz_datum = DateTime.ParseExact("1969-04-06", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
               new szinesz() { szineszid = 49, keresztnev = "Tom", vezeteknev = "Holland", nemzetiseg = "brit", szemszin = "barna", sz_datum = DateTime.ParseExact("1996-06-01", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture) },
            };
            mockedActorrepo.Setup(x => x.GetAll()).Returns(actorList);

            List<szereples> roleList = new List<szereples>()
            {
                new szereples() { filmid = 1, szineszid = 6, szerep = "Tony Stark" },
                new szereples() { filmid = 1, szineszid = 7, szerep = "Pepper Potts" },
                new szereples() { filmid = 4, szineszid = 17, szerep = "Thor" },
                new szereples() { filmid = 5, szineszid = 1, szerep = "Amerika Kapitány" },
                new szereples() { filmid = 5, szineszid = 3, szerep = "Nick Fury" },
            };

            mockedRolerepo.Setup(x => x.GetAll()).Returns(roleList);

            List<film> movieList = new List<film>()
            {
                new film() { filmid = 1, cim = "A Vasember", kijovetel = DateTime.ParseExact("2008-05-02", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 585000000, cselekmeny = "Tony Stark, a zseniális feltaláló és különc milliárdos éppen legújabb szuperfegyverét mutatja be.", hossz = 126, rendezoid = 1, univerzumid = 2 },
                new film() { filmid = 5, cim = "Amerika Kapitány: Az első bosszúálló", kijovetel = DateTime.ParseExact("2011-07-22", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 371000000, cselekmeny = "Javában dúl a második világháború 1941-ben.", hossz = 124, rendezoid = 4, univerzumid = 2 },
                new film() { filmid = 38, cim = "Zöld Lámpás", kijovetel = DateTime.ParseExact("2011-08-11", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 345000000, cselekmeny = "A Zöld Lámpás Alakulat tagjainak feladata a világűr békéjének őrzése.", hossz = 114, rendezoid = 25, univerzumid = 1 },
                new film() { filmid = 36, cim = "A sötét lovag", kijovetel = DateTime.ParseExact("2008-08-07", "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture), bevetel = 345000000, cselekmeny = "Batman (Christian Bale), Gordon felügyelő (Gary Oldman) és a megvesztegethetetlen ügyész, Harvey Dent (Aaron Eckhart) hatásos hadjáratot indítanak a bűnözők ellen.", hossz = 152, rendezoid = 24, univerzumid = 1 },
            };

            mockedMovierepo.Setup(x => x.GetAll()).Returns(movieList);

            Assert.That(this.comicsLogic.GivenActorMovies("Chris Evans").First().Equals("Amerika Kapitány: Az első bosszúálló"));
            Assert.That(this.comicsLogic.GivenActorMovies("Chris Evans").Count().Equals(1));
        }
    }
}
