﻿// <copyright file="JavaEndpoint.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.JavaWeb
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// The JavaEndpoint class.
    /// </summary>
    public class JavaEndpoint
    {
        /// <summary>
        /// JavaEndpoint method.
        /// </summary>
        public void AdatLekero()
        {
            Console.WriteLine("Milyen betűt/szót/ nevet tartalmazó névre keresel?");
            string name = Console.ReadLine();
            Console.WriteLine("Milyen fizetés felett keresel az illetőre? (Adj meg egy számot)");
            int salary = int.Parse(Console.ReadLine());

            string url = $"http://localhost:8080/ComicsMovies.JavaWeb/DirectorFilter?directorname={name}&directorsalary={salary}";
            XDocument xDoc = XDocument.Load(url);

            Console.WriteLine("\nAz ajánlott rendezők listája:");
            foreach (var rendezo in xDoc.Descendants("directors"))
            {
                Console.WriteLine("{0} {1} ({2})", rendezo.Element("keresztnev").Value, rendezo.Element("vezeteknev").Value, rendezo.Element("fizetes").Value);
            }
        }
    }
}
