﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using ComicsMovies.Data;
    using ComicsMovies.JavaWeb;
    using ComicsMovies.Logic;
    using ComicsMovies.Repository;

    /// <summary>
    /// The program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// WriteToConsole method.
        /// </summary>
        /// <typeparam name="T">T.</typeparam>
        /// <param name="collection">Ienumerable collection.</param>
        public static void WriteToConsole<T>(IEnumerable<T> collection)
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }

        /// <summary>
        /// Opens the Main Menu and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">the logic object.</param>
        public static void MainMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();

            string welcome = "Welcome!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n");

            string option = "Please select an option to continue!";
            Console.SetCursorPosition((Console.WindowWidth - option.Length) / 2, Console.CursorTop);
            Console.WriteLine(option + "\n\n\n");

            string actorMenu = "Press {1} to work with Actors";
            Console.SetCursorPosition((Console.WindowWidth - actorMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorMenu + "\n");

            string directorMenu = "Press {2} to work with Directors";
            Console.SetCursorPosition((Console.WindowWidth - directorMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(directorMenu + "\n");

            string movieMenu = "Press {3} to work with Movies";
            Console.SetCursorPosition((Console.WindowWidth - movieMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(movieMenu + "\n");

            string roleMenu = "Press {4} to work with Roles";
            Console.SetCursorPosition((Console.WindowWidth - roleMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(roleMenu + "\n");

            string queriesMenu = "Press {5} to make Queries";
            Console.SetCursorPosition((Console.WindowWidth - queriesMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(queriesMenu + "\n");

            string javaMenu = "Press {6} to Java";
            Console.SetCursorPosition((Console.WindowWidth - javaMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(javaMenu + "\n");

            string quitMenu = "Press {7} to quit";
            Console.SetCursorPosition((Console.WindowWidth - quitMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(quitMenu + "\n");
            char anserw = Console.ReadKey().KeyChar;

            switch (anserw)
            {
                case '1':
                    ActorMenu(comicsLogic);
                    break;
                case '2':
                    DirectorMenu(comicsLogic);
                    break;
                case '3':
                    MovieMenu(comicsLogic);
                    break;
                case '4':
                    RoleMenu(comicsLogic);
                    break;
                case '5':
                    QueriesMenu(comicsLogic);
                    break;
                case '6':
                    JavaMenu(comicsLogic);
                    break;
                case '7':
                    Console.Clear();
                    Console.WriteLine("Goodbye!");
                    Quit();
                    break;
            }
        }

        /// <summary>
        /// Open the Actor Menu, and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">The Logic object.</param>
        public static void ActorMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();
            string welcome = "Please select an option to continue!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n\n\n");

            string oneActor = "Press {1} to get one of the Actors";
            Console.SetCursorPosition((Console.WindowWidth - oneActor.Length) / 2, Console.CursorTop);
            Console.WriteLine(oneActor + "\n");

            string actorList = "Press {2} to list all Actors";
            Console.SetCursorPosition((Console.WindowWidth - actorList.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorList + "\n");

            string actorModify = "Press {3} to modify any of the Actor's last name";
            Console.SetCursorPosition((Console.WindowWidth - actorModify.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorModify + "\n");

            string actorAdd = "Press {4} to add a new Actor";
            Console.SetCursorPosition((Console.WindowWidth - actorAdd.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorAdd + "\n");

            string actorDelete = " Press {5} to delete any Actor";
            Console.SetCursorPosition((Console.WindowWidth - actorDelete.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorDelete + "\n");

            string mainMenu = "Press {6} to main menu ";
            Console.SetCursorPosition((Console.WindowWidth - mainMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(mainMenu + "\n");

            char anserw = Console.ReadKey().KeyChar;
            List<string> actorsWithNames = comicsLogic.ListActorsWithNames();

            switch (anserw)
            {
                case '1':
                    Console.Clear();
                    Console.Write("Adj meg egy azonosítót: ");
                    int azonosito = int.Parse(Console.ReadLine());
                    szinesz sz = comicsLogic.GetOneActor(azonosito);
                    Console.WriteLine(sz.keresztnev + " " + sz.vezeteknev + " | " + sz.sz_datum.Year + "." + sz.sz_datum.Month + "." + sz.sz_datum.Day + " | " + sz.nemzetiseg + " | " + sz.szemszin);
                    Console.WriteLine("Press any key to continue...");
                    string any0 = Console.ReadKey().ToString();

                    if (any0 != null)
                    {
                        ActorMenu(comicsLogic);
                    }

                    break;

                case '2':
                    Console.Clear();
                    Console.WriteLine(" NAME| BIRTHYEAR | NATIONALITY | EYE COLOR");
                    List<szinesz> actors = comicsLogic.ListActors();
                    foreach (var actor in actors)
                    {
                        Console.WriteLine(actor.keresztnev + " " + actor.vezeteknev + " | " + actor.sz_datum.Year + " | " + actor.nemzetiseg + " | " + actor.szemszin);
                    }

                    Console.WriteLine("Press any key to continue...");
                    string any = Console.ReadKey().ToString();

                    if (any != null)
                    {
                        ActorMenu(comicsLogic);
                    }

                    break;

                case '3':
                    Console.Clear();
                    WriteToConsole(actorsWithNames);
                    Console.Write("Please give the Actor's name, wich have a new last name: ");
                    string actorName = Console.ReadLine();
                    Console.Write("Please give the new last name: ");
                    string newLastName = Console.ReadLine();

                    try
                    {
                        comicsLogic.UpdateActorLastName(actorName, newLastName);
                        Console.WriteLine("\n" + actorName + " was sucsessfully modified!");
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("You are now directed back to the Actor Menu");
                    }
                    catch (GivenEntityNotInDatabaseException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("You are now directed back to the Actor Menu");
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        ActorMenu(comicsLogic);
                    }

                    break;
                case '4':
                    Console.Clear();
                    Console.Write("Please give the new Actor's first name: ");
                    string f_name = Console.ReadLine();
                    Console.Write("Please give the new Actor's last name: ");
                    string l_name = Console.ReadLine();
                    Console.Write("Please give the new Actors's nation: ");
                    string nation = Console.ReadLine();
                    Console.Write("Please give the new Actor's birth: ");
                    string birth = Console.ReadLine();
                    Console.Write("For the last, but not least, please give the new Actor's eye color: ");
                    string eyeColor = Console.ReadLine();

                    try
                    {
                        comicsLogic.AddActor(f_name, l_name, nation, birth, eyeColor);
                        Console.WriteLine(f_name + " " + l_name + " was sucsessfully added");
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        ActorMenu(comicsLogic);
                    }

                    break;

                case '5':
                    Console.Clear();
                    WriteToConsole(actorsWithNames);
                    Console.Write("Please give the Actor you want to delete: ");
                    string namedelete = Console.ReadLine();
                    Console.Write("Are you sure, you want to delete " + namedelete + "? [Y/N]");

                    string actordeleteanserw = Console.ReadKey().KeyChar.ToString();
                    try
                    {
                        if (actordeleteanserw.ToUpper() == "Y")
                        {
                            comicsLogic.DeleteActor(namedelete);
                            Console.WriteLine(namedelete + " was sucsessfully deleted!");
                            Thread.Sleep(1000);
                            ActorMenu(comicsLogic);
                        }
                        else if (actordeleteanserw.ToUpper() == "N")
                        {
                            ActorMenu(comicsLogic);
                        }
                        else
                        {
                            Console.WriteLine("You gave a wrong answer, you are now directed back to the Actor Menu");
                            ActorMenu(comicsLogic);
                        }
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine("\n" + e.Message);
                        Console.WriteLine("You are now directed back to the Actor Menu");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("\n" + e.Message);
                        Console.WriteLine("You are now directed back to the Actor Menu");
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        ActorMenu(comicsLogic);
                    }

                    break;

                case '6':

                    MainMenu(comicsLogic);
                    break;
            }
        }

        /// <summary>
        /// Open the Director Menu, and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">The Logic object.</param>
        public static void DirectorMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();

            string welcome = "Please select an option to continue!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n\n\n");

            string oneDirector = "Press {1} to get one of the Directors";
            Console.SetCursorPosition((Console.WindowWidth - oneDirector.Length) / 2, Console.CursorTop);
            Console.WriteLine(oneDirector + "\n");

            string directorList = "Press {2} to list all the Directors";
            Console.SetCursorPosition((Console.WindowWidth - directorList.Length) / 2, Console.CursorTop);
            Console.WriteLine(directorList + "\n");

            string directorModify = "Press {3} to modify any of the Director's salary";
            Console.SetCursorPosition((Console.WindowWidth - directorModify.Length) / 2, Console.CursorTop);
            Console.WriteLine(directorModify + "\n");

            string directorAdd = "Press {4} to add a new Director";
            Console.SetCursorPosition((Console.WindowWidth - directorAdd.Length) / 2, Console.CursorTop);
            Console.WriteLine(directorAdd + "\n");

            string directorDelete = " Press {5} to delete any of the Directors";
            Console.SetCursorPosition((Console.WindowWidth - directorDelete.Length) / 2, Console.CursorTop);
            Console.WriteLine(directorDelete + "\n");

            string mainMenu = "Press {6} to main menu ";
            Console.SetCursorPosition((Console.WindowWidth - mainMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(mainMenu + "\n");

            char anserw = Console.ReadKey().KeyChar;
            List<string> directorsWithNames = comicsLogic.ListDirectorsWithNames();

            switch (anserw)
            {
                case '1':
                    Console.Clear();
                    Console.Write("Adj meg egy azonosítót: ");
                    int azonosito = int.Parse(Console.ReadLine());
                    rendezo r = comicsLogic.GetOneDirector(azonosito);
                    Console.WriteLine(r.keresztnev + " " + r.vezeteknev + " | " + r.sz_datum.Year + "." + r.sz_datum.Month + "." + r.sz_datum.Day + " | " + r.nemzetiseg + " | " + r.fizetes);
                    Console.WriteLine("Press any key to continue...");
                    string any0 = Console.ReadKey().ToString();

                    if (any0 != null)
                    {
                        DirectorMenu(comicsLogic);
                    }

                    break;

                case '2':
                    Console.Clear();
                    Console.WriteLine(" NAME| BIRTHYEAR | NATIONALITY | SALARY ");
                    List<rendezo> directors = comicsLogic.ListDirectors();
                    foreach (var item in directors)
                    {
                        Console.WriteLine(item.keresztnev + " " + item.vezeteknev + " | " + item.sz_datum.Year + " | " + item.nemzetiseg + " | " + item.fizetes);
                    }

                    Console.WriteLine("\n Press any key to continue...");

                    string listend = Console.ReadKey().KeyChar.ToString();

                    if (listend != null)
                    {
                        DirectorMenu(comicsLogic);
                    }

                    break;

                case '3':
                    Console.Clear();
                    WriteToConsole(directorsWithNames);
                    Console.Write("Please give the Director's name, wich have a new salary: ");
                    string directorName = Console.ReadLine();
                    Console.Write("Please give the new salary: ");
                    string newSalary = Console.ReadLine();

                    try
                    {
                        comicsLogic.UpdateDirectorSalary(directorName, newSalary);
                        Console.WriteLine("\n" + directorName + " was sucsessfully modified!");
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("You are now directed back to the Director Menu");
                    }
                    catch (GivenEntityNotInDatabaseException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("You are now directed back to the Director Menu");
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        DirectorMenu(comicsLogic);
                    }

                    break;

                case '4':
                    Console.Clear();

                    Console.Write("Please give the new Director's first name: ");
                    string fname = Console.ReadLine();
                    Console.Write("Please give the new Director's last name: ");
                    string lname = Console.ReadLine();
                    Console.Write("Please give the new Director's nation: ");
                    string nation = Console.ReadLine();
                    Console.Write("Please give the new Director's salary: ");
                    string salar = Console.ReadLine();
                    Console.Write("Please give the new Director's birth: ");
                    string birth = Console.ReadLine();

                    try
                    {
                        comicsLogic.AddDirector(fname, lname, nation, birth, salar);
                        Console.WriteLine(fname + " " + lname + " was sucsessfully added!");
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Something went wrong. Please try again!");
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        DirectorMenu(comicsLogic);
                    }

                    break;

                case '5':
                    Console.Clear();
                    WriteToConsole(directorsWithNames);
                    Console.Write("Please give the Director you want to delete: ");
                    string namedelete = Console.ReadLine();
                    Console.Write("Are you sure, you want to delete " + namedelete + "? [Y/N]");

                    string teamdeleteanserw = Console.ReadKey().KeyChar.ToString();

                    try
                    {
                        if (teamdeleteanserw.ToUpper() == "Y")
                        {
                            comicsLogic.DeleteDirector(namedelete);
                            Console.WriteLine("\n" + namedelete + " was sucsessfully deleted!");
                            Thread.Sleep(1000);
                            DirectorMenu(comicsLogic);
                        }
                        else if (teamdeleteanserw.ToUpper() == "N")
                        {
                            DirectorMenu(comicsLogic);
                        }
                        else
                        {
                            Console.WriteLine("You gave a wrong answer, you are now directed back to the Director Menu");
                            DirectorMenu(comicsLogic);
                        }
                    }
                    catch (GivenEntityNotInDatabaseException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Now you are directed back to the Director Menu");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine("Something went wrong. Please try again!");
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        DirectorMenu(comicsLogic);
                    }

                    break;
                case '6':
                    MainMenu(comicsLogic);
                    break;
            }
        }

        /// <summary>
        /// Open the Movie Menu, and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">The Logic object.</param>
        public static void MovieMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();
            string welcome = "Please select an option to continue!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n\n\n");

            string oneMovie = "Press {1} to get one of the Movies";
            Console.SetCursorPosition((Console.WindowWidth - oneMovie.Length) / 2, Console.CursorTop);
            Console.WriteLine(oneMovie + "\n");

            string movieList = "Press {2} to list all Movies";
            Console.SetCursorPosition((Console.WindowWidth - movieList.Length) / 2, Console.CursorTop);
            Console.WriteLine(movieList + "\n");

            string movieUpdate = "Press {3} to modify any of the movie's title";
            Console.SetCursorPosition((Console.WindowWidth - movieUpdate.Length) / 2, Console.CursorTop);
            Console.WriteLine(movieUpdate + "\n");

            string movieAdd = "Press {4} to add a new Movie";
            Console.SetCursorPosition((Console.WindowWidth - movieAdd.Length) / 2, Console.CursorTop);
            Console.WriteLine(movieAdd + "\n");

            string movieDelete = " Press {5} to delete any Movie";
            Console.SetCursorPosition((Console.WindowWidth - movieDelete.Length) / 2, Console.CursorTop);
            Console.WriteLine(movieDelete + "\n");

            string mainMenu = "Press {6} to main menu ";
            Console.SetCursorPosition((Console.WindowWidth - mainMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(mainMenu + "\n");

            char anserw = Console.ReadKey().KeyChar;
            List<string> movieTitles = comicsLogic.ListMoviesWithTitle();

            switch (anserw)
            {
                case '1':
                    Console.Clear();
                    Console.Write("Adj meg egy azonosítót: ");
                    int azonosito = int.Parse(Console.ReadLine());
                    film f = comicsLogic.GetOneMovie(azonosito);
                    Console.WriteLine(f.cim + " | " + f.hossz + " | " + f.kijovetel?.Year + " | " + f.bevetel + " | " + f.cselekmeny);
                    Console.WriteLine("Press any key to continue...");
                    string any0 = Console.ReadKey().ToString();

                    if (any0 != null)
                    {
                        MovieMenu(comicsLogic);
                    }

                    break;

                case '2':
                    Console.Clear();
                    Console.WriteLine("TITLE | LENTGH | RELEASE YEAR | INCOMING | PLOT");
                    List<film> movies = comicsLogic.ListMovies();
                    foreach (var item in movies)
                    {
                        Console.WriteLine(item.cim + " | " + item.hossz + " | " + item.kijovetel?.Year + " | " + item.bevetel + " | " + item.cselekmeny);
                    }

                    Console.WriteLine("Press any button to continue...");
                    string any = Console.ReadKey().ToString();

                    if (any != null)
                    {
                        MovieMenu(comicsLogic);
                    }

                    break;

                case '3':
                    Console.Clear();
                    WriteToConsole(movieTitles);
                    Console.Write("Please give the movie's title you want to update: ");
                    string oldTitle = Console.ReadLine();
                    Console.Write("Please give the new title of the movie: ");
                    string newTitle = Console.ReadLine();
                    try
                    {
                        comicsLogic.UpdateMovieTitle(oldTitle, newTitle);
                        Console.WriteLine("\n" + oldTitle + " was sucsessfully updated!");
                    }
                    catch (GivenEntityNotInDatabaseException e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    Thread.Sleep(1000);
                    MovieMenu(comicsLogic);
                    break;

                case '4':
                    Console.Clear();

                    Console.Write("Please give the new movie's title: ");
                    string newname = Console.ReadLine();
                    Console.Write("Please give the new movie's release date: ");
                    string relYear = Console.ReadLine();
                    Console.Write("Please give the new movie's length: ");
                    string length = Console.ReadLine();
                    Console.Write("Please give the new movie's director's name: ");
                    string dirName = Console.ReadLine();
                    Console.Write("Please give the new movie's universe name: ");
                    string unName = Console.ReadLine();
                    Console.Write("Please give the new movie's incoming: ");
                    string incoming = Console.ReadLine();
                    Console.Write("Please give the new movie's plot: ");
                    string plot = Console.ReadLine();
                    try
                    {
                        comicsLogic.AddMovie(newname, relYear, length, plot, unName, dirName, incoming);
                        Console.WriteLine("Movie added successfully!");
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        MovieMenu(comicsLogic);
                    }

                    break;

                case '5':
                    Console.Clear();
                    WriteToConsole(movieTitles);
                    Console.Write("Please give the movies's name you want to delete: ");
                    string deletename = Console.ReadLine();
                    Console.Write("Are you sure you want to delete " + deletename + "? [Y|N]");
                    string deleteany = Console.ReadKey().KeyChar.ToString();
                    try
                    {
                        if (deleteany.ToUpper() == "Y")
                        {
                            comicsLogic.DeleteMovie(deletename);
                            Console.WriteLine(deletename + " was sucsessfully deleted!");
                            Thread.Sleep(1000);
                            MovieMenu(comicsLogic);
                        }
                        else if (deleteany.ToUpper() == "N")
                        {
                            Console.WriteLine("You are directed back to the Movie Menu");

                            Thread.Sleep(1000);
                            MovieMenu(comicsLogic);
                        }
                        else
                        {
                            Console.WriteLine("You gave  wrong answer, now you are directed back to the Movie Menu");
                        }
                    }
                    catch (GivenEntityNotInDatabaseException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        MovieMenu(comicsLogic);
                    }

                    break;
                case '6':
                    MainMenu(comicsLogic);
                    break;
            }
        }

        /// <summary>
        /// Open the Role Menu, and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">The Logic object.</param>
        public static void RoleMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();
            string welcome = "Please select an option to continue!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n\n\n");

            string oneRole = "Press {1} to get one of the Roles";
            Console.SetCursorPosition((Console.WindowWidth - oneRole.Length) / 2, Console.CursorTop);
            Console.WriteLine(oneRole + "\n");

            string roleList = "Press {2} to list all Roles";
            Console.SetCursorPosition((Console.WindowWidth - roleList.Length) / 2, Console.CursorTop);
            Console.WriteLine(roleList + "\n");

            string roleDelete = " Press {3} to delete any Role";
            Console.SetCursorPosition((Console.WindowWidth - roleDelete.Length) / 2, Console.CursorTop);
            Console.WriteLine(roleDelete + "\n");

            string roleAdd = " Press {4} to add a new Role";
            Console.SetCursorPosition((Console.WindowWidth - roleAdd.Length) / 2, Console.CursorTop);
            Console.WriteLine(roleAdd + "\n");

            string mainMenu = "Press {5} to main menu ";
            Console.SetCursorPosition((Console.WindowWidth - mainMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(mainMenu + "\n");

            char anserw = Console.ReadKey().KeyChar;
            List<string> roles = comicsLogic.ListRoleEntitiesWithNames();
            List<string> actorList = new List<string>();
            List<string> rolesList = new List<string>();
            List<string> movieList = new List<string>();

            switch (anserw)
            {
                case '1':
                    Console.Clear();
                    try
                    {
                        Console.Write("Adj meg egy film azonosítót: ");
                        int filmazonosito = int.Parse(Console.ReadLine());
                        Console.Write("Adj meg egy színész azonosítót: ");
                        int szineszazonosito = int.Parse(Console.ReadLine());
                        szereples sz = comicsLogic.GetOneRole(filmazonosito, szineszazonosito);
                        Console.WriteLine(sz.szerep);
                        Console.WriteLine("Press any key to continue...");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        RoleMenu(comicsLogic);
                    }

                    break;

                case '2':
                    Console.Clear();
                    Console.WriteLine("ACTOR - MOVIE TITLE - ROLE");
                    int i = 0;
                    foreach (var item in roles)
                    {
                        i++;
                        if (i % 3 == 1)
                        {
                            actorList.Add(item);
                        }
                        else if (i % 3 == 2)
                        {
                            movieList.Add(item);
                        }
                        else
                        {
                            rolesList.Add(item);
                        }
                    }

                    for (int x = 0; x < actorList.Count; x++)
                    {
                        Console.WriteLine(actorList[x] + " - " + movieList[x] + " - " + rolesList[x]);
                    }

                    Console.WriteLine("Press any key to continue...");

                    string any = Console.ReadKey().ToString();

                    if (any != null)
                    {
                        RoleMenu(comicsLogic);
                    }

                    break;

                case '3':
                    Console.Clear();
                    Console.Write("Please give the Actor's name and the movie he or she was in which you want to delete: \n");
                    Console.WriteLine("First, give the actor's full name:");
                    string actorName = Console.ReadLine();
                    Console.Write("Then give the movie's title:");
                    string movieTitle = Console.ReadLine();
                    Console.Write("Are you sure, you want to delete " + actorName + " in the following movie:  " + movieTitle + "? [Y/N]");

                    string deleteanserw = Console.ReadKey().KeyChar.ToString();

                    try
                    {
                        if (deleteanserw.ToUpper() == "Y")
                        {
                            comicsLogic.DeleteRole(movieTitle, actorName);
                            Console.WriteLine("This Role was sucsessfully deleted!");
                            Thread.Sleep(1000);
                            RoleMenu(comicsLogic);
                        }
                        else if (deleteanserw.ToUpper() == "N")
                        {
                            RoleMenu(comicsLogic);
                        }
                        else
                        {
                            Console.WriteLine("You gave a wrong answer you are now directed back to the Role Menu");
                            RoleMenu(comicsLogic);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        RoleMenu(comicsLogic);
                    }

                    break;

                case '4':
                    Console.Clear();
                    List<string> movies = comicsLogic.ListMoviesWithTitle();
                    WriteToConsole(movies);
                    Console.Write("Give the movie's name: ");
                    string title = Console.ReadLine();
                    Console.Clear();
                    List<string> actors = comicsLogic.ListActorsWithNames();
                    WriteToConsole(actors);
                    Console.Write("Give the actor's name: ");
                    string acto = Console.ReadLine();
                    Console.Write("Give the role: ");
                    string role = Console.ReadLine();
                    try
                    {
                        comicsLogic.AddRole(title, acto, role);
                        Console.WriteLine("Role added!");
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Thread.Sleep(1000);
                        RoleMenu(comicsLogic);
                    }

                    break;

                case '5':
                    MainMenu(comicsLogic);
                    break;
            }
        }

        /// <summary>
        /// Open the Queries Menu, and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">The Logic object.</param>
        public static void QueriesMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();

            string welcome = "Please select an option to continue!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n\n\n");

            string actorsMovies = "Press {1} to get an actor's movies";
            Console.SetCursorPosition((Console.WindowWidth - actorsMovies.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorsMovies + "\n");

            string averageIncom = "Press {2} to get the average incoming group by universe";
            Console.SetCursorPosition((Console.WindowWidth - averageIncom.Length) / 2, Console.CursorTop);
            Console.WriteLine(averageIncom + "\n");

            string actorInMovies = "Press {3} to get how many movies were the actors in";
            Console.SetCursorPosition((Console.WindowWidth - actorInMovies.Length) / 2, Console.CursorTop);
            Console.WriteLine(actorInMovies + "\n");

            string mainMenu = "Press {4} to Main Menu";
            Console.SetCursorPosition((Console.WindowWidth - mainMenu.Length) / 2, Console.CursorTop);
            Console.WriteLine(mainMenu + "\n");

            char anserw = Console.ReadKey().KeyChar;

            switch (anserw)
            {
                case '1':
                    Console.Clear();
                    List<string> actors = comicsLogic.ListActorsWithNames();
                    WriteToConsole(actors);
                    Console.Write("Please give the actor's name:");
                    string name = Console.ReadLine();
                    Console.Clear();
                    List<string> movies = comicsLogic.GivenActorMovies(name);
                    WriteToConsole(movies);
                    Console.WriteLine("Press any key to continue...");
                    string any = Console.ReadKey().ToString();
                    if (any != null)
                    {
                        QueriesMenu(comicsLogic);
                    }

                    break;

                case '2':
                    Console.Clear();
                    List<string> avInc = comicsLogic.AverageIncomingGroupByUniverse();
                    List<string> universe = new List<string>();
                    List<string> incomings = new List<string>();
                    int i = 0;
                    foreach (var item in avInc)
                    {
                        i++;
                        if (i % 2 == 1)
                        {
                            universe.Add(item);
                        }
                        else
                        {
                            incomings.Add(item);
                        }
                    }

                    for (int x = 0; x < universe.Count; x++)
                    {
                        Console.WriteLine(universe[x] + " - " + incomings[x]);
                    }

                    Console.WriteLine("Press any key to continue...");
                    string an = Console.ReadKey().ToString();
                    if (an != null)
                    {
                        QueriesMenu(comicsLogic);
                    }

                    break;

                case '3':
                    Console.Clear();
                    List<string> actsInMovies = comicsLogic.ActorsInHowManyMovies();
                    List<string> actor_s = new List<string>();
                    List<string> counts = new List<string>();

                    int j = 0;
                    foreach (var item in actsInMovies)
                    {
                        j++;
                        if (j % 2 == 1)
                        {
                            actor_s.Add(item);
                        }
                        else
                        {
                            counts.Add(item);
                        }
                    }

                    for (int h = 0; h < actor_s.Count; h++)
                    {
                        Console.WriteLine(actor_s[h] + " | " + counts[h]);
                    }

                    Console.WriteLine("Press any key to continue...");
                    string anew = Console.ReadKey().ToString();
                    if (anew != null)
                    {
                        QueriesMenu(comicsLogic);
                    }

                    break;

                case '4':
                    MainMenu(comicsLogic);
                    break;
            }
        }

        /// <summary>
        /// Opens the Java Menu and writes its elements to the Console.
        /// </summary>
        /// <param name="comicsLogic">The logic object.</param>
        public static void JavaMenu(ComicsLogic comicsLogic)
        {
            Console.Clear();
            string welcome = "Hello!";
            Console.SetCursorPosition((Console.WindowWidth - welcome.Length) / 2, Console.CursorTop);
            Console.WriteLine(welcome + "\n\n\n");
            try
            {
                JavaEndpoint je = new JavaEndpoint();
                je.AdatLekero();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MainMenu(comicsLogic);
            }
        }

        /// <summary>
        /// Quit from the program.
        /// </summary>
        public static void Quit()
        {
            Thread.Sleep(1000);
            Environment.Exit(0);
        }

        /// <summary>
        /// The program.
        /// </summary>
        /// <param name="args">args.</param>
        private static void Main(string[] args)
        {
            ActorRepository actorrepo = new ActorRepository();
            MovieRepository movierepo = new MovieRepository();
            RoleRepository rolerepo = new RoleRepository();
            DirectorRepository directorrepo = new DirectorRepository();
            ComicsLogic comicsLogic = new ComicsLogic(movierepo, directorrepo, actorrepo, rolerepo);
            MainMenu(comicsLogic);
        }
    }
}
