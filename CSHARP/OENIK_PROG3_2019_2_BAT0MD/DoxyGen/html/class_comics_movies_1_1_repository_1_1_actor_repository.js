var class_comics_movies_1_1_repository_1_1_actor_repository =
[
    [ "ActorRepository", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a48c6e0955fe3d4ddb1f4533580974844", null ],
    [ "Add", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a29e516502a00388b812232442bd1b64c", null ],
    [ "DeleteActor", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a79562d5f7b3c31d2488046663735fee5", null ],
    [ "GetAll", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a5abdb74ec0b74d0015d40ad255814c0b", null ],
    [ "GetOne", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a5fd3221bd3e413196e5f5cc6f6fd1bc4", null ],
    [ "IsInDatabase", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a12b23adeab7fc182205bc6d6e8ab153c", null ],
    [ "ListActorsWithNames", "class_comics_movies_1_1_repository_1_1_actor_repository.html#a13d50d8357dbcbdc437ec2e19a04b982", null ],
    [ "UpdateActorLastName", "class_comics_movies_1_1_repository_1_1_actor_repository.html#ab12bf3d0760ddf2a822b7f93b2c19a2b", null ]
];