var dir_74c4aa9d56ce8c56799736c4a7cd55ac =
[
    [ "ComicsMovies.Data", "dir_ba7e1549f4916b99a3a3f65c25705392.html", "dir_ba7e1549f4916b99a3a3f65c25705392" ],
    [ "ComicsMovies.JavaWeb", "dir_6d149ba278e38419ee211bb17f253142.html", "dir_6d149ba278e38419ee211bb17f253142" ],
    [ "ComicsMovies.Logic", "dir_482be32a3fedbe7659b6156873ff6352.html", "dir_482be32a3fedbe7659b6156873ff6352" ],
    [ "ComicsMovies.Logic.Tests", "dir_6ffd7e46b6dc7313bd8cd7c583e60193.html", "dir_6ffd7e46b6dc7313bd8cd7c583e60193" ],
    [ "ComicsMovies.Program", "dir_ea4862426c15eb8bf8b1713cdc078097.html", "dir_ea4862426c15eb8bf8b1713cdc078097" ],
    [ "ComicsMovies.Repository", "dir_04f12c23c5eaa0632a91acc1d695cadb.html", "dir_04f12c23c5eaa0632a91acc1d695cadb" ]
];