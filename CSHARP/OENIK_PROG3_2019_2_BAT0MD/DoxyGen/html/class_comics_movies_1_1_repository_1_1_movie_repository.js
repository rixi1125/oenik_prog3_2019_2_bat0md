var class_comics_movies_1_1_repository_1_1_movie_repository =
[
    [ "MovieRepository", "class_comics_movies_1_1_repository_1_1_movie_repository.html#a544a0c1b4a4007d650c7c2cbb45b27a6", null ],
    [ "Add", "class_comics_movies_1_1_repository_1_1_movie_repository.html#a6949a0039cf4bcb0c69de6d314a2548c", null ],
    [ "DeleteMovie", "class_comics_movies_1_1_repository_1_1_movie_repository.html#ab2163edbe21136afeef96184854d8462", null ],
    [ "GetAll", "class_comics_movies_1_1_repository_1_1_movie_repository.html#a972ed987a37c3676937d94876b62cb6b", null ],
    [ "GetOne", "class_comics_movies_1_1_repository_1_1_movie_repository.html#a76fc8bec906265a63fe147b172551fde", null ],
    [ "IsInDatabase", "class_comics_movies_1_1_repository_1_1_movie_repository.html#a8148dd25b2ba41100f25a0c43dfeb341", null ],
    [ "ListMoviesWithTitle", "class_comics_movies_1_1_repository_1_1_movie_repository.html#afe1fdc5f44dbc52b853849255d066423", null ],
    [ "UpdateMovieTitle", "class_comics_movies_1_1_repository_1_1_movie_repository.html#a81f43f159b1662b8c1c97993238ec346", null ]
];