var interface_comics_movies_1_1_logic_1_1_i_logic =
[
    [ "ActorsInHowManyMovies", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a438fd9d82725b3e102a570399ec4ecc3", null ],
    [ "AddActor", "interface_comics_movies_1_1_logic_1_1_i_logic.html#af4e0616f4ecb39a1ff7a1d95a0569dde", null ],
    [ "AddDirector", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a841cd65d9b0cd7c183cdc4136a89657e", null ],
    [ "AddMovie", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a41d7ba02179bb9d72994a021b9e54c32", null ],
    [ "AddRole", "interface_comics_movies_1_1_logic_1_1_i_logic.html#aef9e2f9cf5f0a8b4cc56756c3b06e31c", null ],
    [ "AverageIncomingGroupByUniverse", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a5f068656226b0e888dc8f5ee78fe4d6d", null ],
    [ "DeleteActor", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a15451b851b80b62f19097ccd14bbd7e2", null ],
    [ "DeleteDirector", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a4bba4178bced6fa22367ea7ac0484b1f", null ],
    [ "DeleteMovie", "interface_comics_movies_1_1_logic_1_1_i_logic.html#aded10069632ee2385c7a07cb5bec65a2", null ],
    [ "DeleteRole", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a773c1027c5fd32828cf826f98ce5f8dc", null ],
    [ "GivenActorMovies", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a9fa4ad83d79492ca15b93e3c9f175074", null ],
    [ "ListActors", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a830333f2d2d29534f744fa4b256d5611", null ],
    [ "ListDirectors", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a109159a33ff221c74538551851fba2e0", null ],
    [ "ListMovies", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a59153d75238e36f6c0e96758f48cc61f", null ],
    [ "ListRoles", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a7cc2ada4a1ef7ea22831ae5e724e90d0", null ],
    [ "UpdateActorLastName", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a7448c2b47740e01f4f5e3498642990b9", null ],
    [ "UpdateDirectorSalary", "interface_comics_movies_1_1_logic_1_1_i_logic.html#aa4d3f647828a6f76784cf5408fdf08c9", null ],
    [ "UpdateMovieTitle", "interface_comics_movies_1_1_logic_1_1_i_logic.html#a07c7b6a5718b35fc61440945d2e0e17a", null ]
];