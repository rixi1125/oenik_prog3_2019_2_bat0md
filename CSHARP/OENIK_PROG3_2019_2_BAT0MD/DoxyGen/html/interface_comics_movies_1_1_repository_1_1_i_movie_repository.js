var interface_comics_movies_1_1_repository_1_1_i_movie_repository =
[
    [ "DeleteMovie", "interface_comics_movies_1_1_repository_1_1_i_movie_repository.html#aed2d629ccfea27425449bd31b7b40025", null ],
    [ "GetOne", "interface_comics_movies_1_1_repository_1_1_i_movie_repository.html#a56429837557cd74de1ffb5b94b277a8d", null ],
    [ "UpdateMovieTitle", "interface_comics_movies_1_1_repository_1_1_i_movie_repository.html#a3b9116901d72fa46ed2a67f742d6f26c", null ]
];