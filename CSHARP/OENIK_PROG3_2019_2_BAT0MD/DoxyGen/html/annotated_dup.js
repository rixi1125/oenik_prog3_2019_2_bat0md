var annotated_dup =
[
    [ "ComicsMovies", null, [
      [ "Data", null, [
        [ "ComicsMoviesDatabaseEntities", "class_comics_movies_1_1_data_1_1_comics_movies_database_entities.html", null ],
        [ "film", "class_comics_movies_1_1_data_1_1film.html", "class_comics_movies_1_1_data_1_1film" ],
        [ "rendezo", "class_comics_movies_1_1_data_1_1rendezo.html", "class_comics_movies_1_1_data_1_1rendezo" ],
        [ "szereples", "class_comics_movies_1_1_data_1_1szereples.html", "class_comics_movies_1_1_data_1_1szereples" ],
        [ "szinesz", "class_comics_movies_1_1_data_1_1szinesz.html", "class_comics_movies_1_1_data_1_1szinesz" ],
        [ "univerzum", "class_comics_movies_1_1_data_1_1univerzum.html", "class_comics_movies_1_1_data_1_1univerzum" ]
      ] ],
      [ "JavaWeb", "namespace_comics_movies_1_1_java_web.html", "namespace_comics_movies_1_1_java_web" ],
      [ "Logic", "namespace_comics_movies_1_1_logic.html", "namespace_comics_movies_1_1_logic" ],
      [ "Repository", "namespace_comics_movies_1_1_repository.html", "namespace_comics_movies_1_1_repository" ]
    ] ]
];