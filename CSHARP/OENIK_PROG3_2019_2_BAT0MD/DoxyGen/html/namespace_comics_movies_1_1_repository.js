var namespace_comics_movies_1_1_repository =
[
    [ "ActorRepository", "class_comics_movies_1_1_repository_1_1_actor_repository.html", "class_comics_movies_1_1_repository_1_1_actor_repository" ],
    [ "DirectorRepository", "class_comics_movies_1_1_repository_1_1_director_repository.html", "class_comics_movies_1_1_repository_1_1_director_repository" ],
    [ "GivenEntityNotInDatabaseException", "class_comics_movies_1_1_repository_1_1_given_entity_not_in_database_exception.html", "class_comics_movies_1_1_repository_1_1_given_entity_not_in_database_exception" ],
    [ "IActorRepository", "interface_comics_movies_1_1_repository_1_1_i_actor_repository.html", "interface_comics_movies_1_1_repository_1_1_i_actor_repository" ],
    [ "IDirectorRepository", "interface_comics_movies_1_1_repository_1_1_i_director_repository.html", "interface_comics_movies_1_1_repository_1_1_i_director_repository" ],
    [ "IMovieRepository", "interface_comics_movies_1_1_repository_1_1_i_movie_repository.html", "interface_comics_movies_1_1_repository_1_1_i_movie_repository" ],
    [ "IRepository", "interface_comics_movies_1_1_repository_1_1_i_repository.html", "interface_comics_movies_1_1_repository_1_1_i_repository" ],
    [ "IRoleRepository", "interface_comics_movies_1_1_repository_1_1_i_role_repository.html", "interface_comics_movies_1_1_repository_1_1_i_role_repository" ],
    [ "MovieRepository", "class_comics_movies_1_1_repository_1_1_movie_repository.html", "class_comics_movies_1_1_repository_1_1_movie_repository" ],
    [ "RoleRepository", "class_comics_movies_1_1_repository_1_1_role_repository.html", "class_comics_movies_1_1_repository_1_1_role_repository" ]
];