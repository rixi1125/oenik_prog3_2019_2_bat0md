var class_comics_movies_1_1_repository_1_1_director_repository =
[
    [ "DirectorRepository", "class_comics_movies_1_1_repository_1_1_director_repository.html#a7cf2b7cbc5c2ceb7dd1130e20a59ecfb", null ],
    [ "Add", "class_comics_movies_1_1_repository_1_1_director_repository.html#a6f81f74f04df7488f5fff6ca3f778681", null ],
    [ "DeleteDirector", "class_comics_movies_1_1_repository_1_1_director_repository.html#aa8ad9e285da8c32826047223721652da", null ],
    [ "GetAll", "class_comics_movies_1_1_repository_1_1_director_repository.html#a24e89722d816b2563de32aa46113ee96", null ],
    [ "IsInDatabase", "class_comics_movies_1_1_repository_1_1_director_repository.html#a5049fab6dee749fdd0ee99079775cbe7", null ],
    [ "ListDirectorsWithNames", "class_comics_movies_1_1_repository_1_1_director_repository.html#a594a9b6b4598787c0b2446f31c4e4abb", null ],
    [ "UpdateDirectorSalary", "class_comics_movies_1_1_repository_1_1_director_repository.html#a3f50d9e5467b1f2fddd52b880a94cc56", null ]
];