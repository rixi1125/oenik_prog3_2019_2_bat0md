var searchData=
[
  ['iactorrepository_80',['IActorRepository',['../interface_comics_movies_1_1_repository_1_1_i_actor_repository.html',1,'ComicsMovies::Repository']]],
  ['idirectorrepository_81',['IDirectorRepository',['../interface_comics_movies_1_1_repository_1_1_i_director_repository.html',1,'ComicsMovies::Repository']]],
  ['ilogic_82',['ILogic',['../interface_comics_movies_1_1_logic_1_1_i_logic.html',1,'ComicsMovies::Logic']]],
  ['imovierepository_83',['IMovieRepository',['../interface_comics_movies_1_1_repository_1_1_i_movie_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_84',['IRepository',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20film_20_3e_85',['IRepository&lt; film &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20rendezo_20_3e_86',['IRepository&lt; rendezo &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20szereples_20_3e_87',['IRepository&lt; szereples &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20szinesz_20_3e_88',['IRepository&lt; szinesz &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irolerepository_89',['IRoleRepository',['../interface_comics_movies_1_1_repository_1_1_i_role_repository.html',1,'ComicsMovies::Repository']]]
];
