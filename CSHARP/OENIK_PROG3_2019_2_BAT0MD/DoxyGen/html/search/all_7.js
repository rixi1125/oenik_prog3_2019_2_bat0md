var searchData=
[
  ['iactorrepository_33',['IActorRepository',['../interface_comics_movies_1_1_repository_1_1_i_actor_repository.html',1,'ComicsMovies::Repository']]],
  ['idirectorrepository_34',['IDirectorRepository',['../interface_comics_movies_1_1_repository_1_1_i_director_repository.html',1,'ComicsMovies::Repository']]],
  ['ilogic_35',['ILogic',['../interface_comics_movies_1_1_logic_1_1_i_logic.html',1,'ComicsMovies::Logic']]],
  ['imovierepository_36',['IMovieRepository',['../interface_comics_movies_1_1_repository_1_1_i_movie_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_37',['IRepository',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20film_20_3e_38',['IRepository&lt; film &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20rendezo_20_3e_39',['IRepository&lt; rendezo &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20szereples_20_3e_40',['IRepository&lt; szereples &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irepository_3c_20szinesz_20_3e_41',['IRepository&lt; szinesz &gt;',['../interface_comics_movies_1_1_repository_1_1_i_repository.html',1,'ComicsMovies::Repository']]],
  ['irolerepository_42',['IRoleRepository',['../interface_comics_movies_1_1_repository_1_1_i_role_repository.html',1,'ComicsMovies::Repository']]],
  ['isindatabase_43',['IsInDatabase',['../class_comics_movies_1_1_repository_1_1_actor_repository.html#a12b23adeab7fc182205bc6d6e8ab153c',1,'ComicsMovies.Repository.ActorRepository.IsInDatabase()'],['../class_comics_movies_1_1_repository_1_1_director_repository.html#a5049fab6dee749fdd0ee99079775cbe7',1,'ComicsMovies.Repository.DirectorRepository.IsInDatabase()'],['../class_comics_movies_1_1_repository_1_1_movie_repository.html#a8148dd25b2ba41100f25a0c43dfeb341',1,'ComicsMovies.Repository.MovieRepository.IsInDatabase()']]]
];
