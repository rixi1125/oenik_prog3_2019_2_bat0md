var class_comics_movies_1_1_repository_1_1_role_repository =
[
    [ "RoleRepository", "class_comics_movies_1_1_repository_1_1_role_repository.html#a9b6ff7e5b277428b94319af223d0f806", null ],
    [ "Add", "class_comics_movies_1_1_repository_1_1_role_repository.html#a1743413203ceb7f3691302fa7523dcec", null ],
    [ "DeleteRole", "class_comics_movies_1_1_repository_1_1_role_repository.html#afe0e9fbdc5bc640ce6204b12874c3cfa", null ],
    [ "GetAll", "class_comics_movies_1_1_repository_1_1_role_repository.html#a4d5321c00816cc90a6aa03b5a21418d3", null ],
    [ "ListRoleEntitiesWithNames", "class_comics_movies_1_1_repository_1_1_role_repository.html#a3b0bd0af1a739599872127cfaf384cdd", null ]
];