var hierarchy =
[
    [ "DbContext", null, [
      [ "ComicsMovies::Data::ComicsMoviesDatabaseEntities", "class_comics_movies_1_1_data_1_1_comics_movies_database_entities.html", null ]
    ] ],
    [ "Exception", null, [
      [ "ComicsMovies.Repository.GivenEntityNotInDatabaseException", "class_comics_movies_1_1_repository_1_1_given_entity_not_in_database_exception.html", null ]
    ] ],
    [ "ComicsMovies.Data.film", "class_comics_movies_1_1_data_1_1film.html", null ],
    [ "ComicsMovies.Logic.ILogic", "interface_comics_movies_1_1_logic_1_1_i_logic.html", [
      [ "ComicsMovies.Logic.ComicsLogic", "class_comics_movies_1_1_logic_1_1_comics_logic.html", null ]
    ] ],
    [ "ComicsMovies.Repository.IRepository< T >", "interface_comics_movies_1_1_repository_1_1_i_repository.html", null ],
    [ "ComicsMovies.Repository.IRepository< film >", "interface_comics_movies_1_1_repository_1_1_i_repository.html", [
      [ "ComicsMovies.Repository.IMovieRepository", "interface_comics_movies_1_1_repository_1_1_i_movie_repository.html", [
        [ "ComicsMovies.Repository.MovieRepository", "class_comics_movies_1_1_repository_1_1_movie_repository.html", null ]
      ] ]
    ] ],
    [ "ComicsMovies.Repository.IRepository< rendezo >", "interface_comics_movies_1_1_repository_1_1_i_repository.html", [
      [ "ComicsMovies.Repository.IDirectorRepository", "interface_comics_movies_1_1_repository_1_1_i_director_repository.html", [
        [ "ComicsMovies.Repository.DirectorRepository", "class_comics_movies_1_1_repository_1_1_director_repository.html", null ]
      ] ]
    ] ],
    [ "ComicsMovies.Repository.IRepository< szereples >", "interface_comics_movies_1_1_repository_1_1_i_repository.html", [
      [ "ComicsMovies.Repository.IRoleRepository", "interface_comics_movies_1_1_repository_1_1_i_role_repository.html", [
        [ "ComicsMovies.Repository.RoleRepository", "class_comics_movies_1_1_repository_1_1_role_repository.html", null ]
      ] ]
    ] ],
    [ "ComicsMovies.Repository.IRepository< szinesz >", "interface_comics_movies_1_1_repository_1_1_i_repository.html", [
      [ "ComicsMovies.Repository.IActorRepository", "interface_comics_movies_1_1_repository_1_1_i_actor_repository.html", [
        [ "ComicsMovies.Repository.ActorRepository", "class_comics_movies_1_1_repository_1_1_actor_repository.html", null ]
      ] ]
    ] ],
    [ "ComicsMovies.JavaWeb.JavaEndpoint", "class_comics_movies_1_1_java_web_1_1_java_endpoint.html", null ],
    [ "ComicsMovies.Data.rendezo", "class_comics_movies_1_1_data_1_1rendezo.html", null ],
    [ "ComicsMovies.Data.szereples", "class_comics_movies_1_1_data_1_1szereples.html", null ],
    [ "ComicsMovies.Data.szinesz", "class_comics_movies_1_1_data_1_1szinesz.html", null ],
    [ "ComicsMovies.Data.univerzum", "class_comics_movies_1_1_data_1_1univerzum.html", null ]
];