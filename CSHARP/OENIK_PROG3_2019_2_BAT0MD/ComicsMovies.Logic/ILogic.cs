﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Logic interface.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Add a new actor to the database.
        /// </summary>
        /// <param name="first_name">actors's first name.</param>
        /// <param name="last_name">actor's last name.</param>
        /// <param name="nationality">actors's nationality.</param>
        /// <param name="birth">birth date.</param>
        /// <param name="eyeColor">actor's eye color.</param>
        void AddActor(string first_name, string last_name, string nationality, string birth, string eyeColor);

        /// <summary>
        /// Add a new director to the database.
        /// </summary>
        /// <param name="first_name">director's first name.</param>
        /// <param name="last_name">director's last name.</param>
        /// <param name="nationality">director's nationality.</param>
        /// <param name="birth">bith date.</param>
        /// <param name="salary">director's salary.</param>
        void AddDirector(string first_name, string last_name, string nationality, string birth, string salary);

        /// <summary>
        /// Add new Movie to database.
        /// </summary>
        /// <param name="title">movie title.</param>
        /// <param name="releaseYear">release year.</param>
        /// <param name="length">movie length.</param>
        /// <param name="plot">plot.</param>
        /// <param name="universeName">universeName.</param>
        /// <param name="directorName">directorName.</param>
        /// <param name="incoming">incoming value.</param>
        void AddMovie(string title, string releaseYear, string length, string plot, string universeName, string directorName, string incoming);

        /// <summary>
        /// Add new role to the database.
        /// </summary>
        /// <param name="movieName">movieName.</param>
        /// <param name="actorName">actorName.</param>
        /// <param name="role">role.</param>
        void AddRole(string movieName, string actorName, string role);

        /// <summary>
        /// Delete an actor.
        /// </summary>
        /// <param name="name">name.</param>
        void DeleteActor(string name);

        /// <summary>
        /// Delete a movie.
        /// </summary>
        /// <param name="title">title.</param>
        void DeleteMovie(string title);

        /// <summary>
        /// Delete a director.
        /// </summary>
        /// <param name="name">name.</param>
        void DeleteDirector(string name);

        /// <summary>
        /// Delete a role.
        /// </summary>
        /// <param name="movieTitle">movieTitle.</param>
        /// <param name="actorName">actorName.</param>
        void DeleteRole(string movieTitle, string actorName);

        /// <summary>
        /// Lists the actors.
        /// </summary>
        /// <returns>an actor list.</returns>
        List<szinesz> ListActors();

        /// <summary>
        /// Lists the directors.
        /// </summary>
        /// <returns>a director list.</returns>
        List<rendezo> ListDirectors();

        /// <summary>
        /// Lists the movies.
        /// </summary>
        /// <returns>a movie list.</returns>
        List<film> ListMovies();

        /// <summary>
        /// Lists the universes.
        /// </summary>
        /// <returns>a list of universes.</returns>
        List<univerzum> ListUniverses();

        /// <summary>
        /// Lists the roles.
        /// </summary>
        /// <returns>a role list.</returns>
        List<szereples> ListRoles();

        /// <summary>
        /// GetOneActor method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>an actor.</returns>
        szinesz GetOneActor(int id);

        /// <summary>
        /// GetOneMovie method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a movie.</returns>
        film GetOneMovie(int id);

        /// <summary>
        /// GetOneRole method.
        /// </summary>
        /// <param name="movieid">movieid.</param>
        /// <param name="actorid">actorid.</param>
        /// <returns>a role.</returns>
        szereples GetOneRole(int movieid, int actorid);

        /// <summary>
        /// GetOneDirector method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a director.</returns>
        rendezo GetOneDirector(int id);

        /// <summary>
        /// Update the actor's last name.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="newLastName">newLastName.</param>
        void UpdateActorLastName(string name, string newLastName);

        /// <summary>
        /// Update the given director's salary.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="newSalary">newSalary.</param>
        void UpdateDirectorSalary(string name, string newSalary);

        /// <summary>
        /// Update movie title.
        /// </summary>
        /// <param name="oldTitle">oldTitle.</param>
        /// <param name="newTitle">newTitle.</param>
        void UpdateMovieTitle(string oldTitle, string newTitle);

        /// <summary>
        /// Gives back an actor's movies.
        /// </summary>
        /// <param name="name">name.</param>
        /// <returns>string list of movies.</returns>
        List<string> GivenActorMovies(string name);

        /// <summary>
        /// Gives back the average incoming group by universe.
        /// </summary>
        /// <returns>string list.</returns>
        List<string> AverageIncomingGroupByUniverse();

        /// <summary>
        /// Gives back how many movies were the actors in.
        /// </summary>
        /// <returns>string list.</returns>
        List<string> ActorsInHowManyMovies();
    }
}
