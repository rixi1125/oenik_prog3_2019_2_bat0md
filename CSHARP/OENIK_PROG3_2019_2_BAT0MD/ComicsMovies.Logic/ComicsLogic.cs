﻿// <copyright file="ComicsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using ComicsMovies.Data;
    using ComicsMovies.Repository;

    /// <summary>
    /// The Logic.
    /// </summary>
    public class ComicsLogic : ILogic
    {
        private readonly IMovieRepository movierepo;
        private readonly IDirectorRepository directorrepo;
        private readonly IActorRepository actorrepo;
        private readonly IRoleRepository rolerepo;
        private readonly UniverseRepository universerepo;
        private ComicsMoviesDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComicsLogic"/> class.
        /// The logic.
        /// </summary>
        /// <param name="movierepo">movierepo.</param>
        /// <param name="directorrepo">directorrepo.</param>
        /// <param name="actorrepo">actorrepo.</param>
        /// <param name="rolerepo">rolerepo.</param>
        public ComicsLogic(IMovieRepository movierepo, IDirectorRepository directorrepo, IActorRepository actorrepo, IRoleRepository rolerepo)
        {
            this.db = new ComicsMoviesDatabaseEntities();
            this.movierepo = movierepo;
            this.directorrepo = directorrepo;
            this.actorrepo = actorrepo;
            this.rolerepo = rolerepo;
            this.universerepo = new UniverseRepository();
        }

        /// <summary>
        /// Add a new actor to the database.
        /// </summary>
        /// <param name="first_name">actors's first name.</param>
        /// <param name="last_name">actor's last name.</param>
        /// <param name="nationality">actors's nationality.</param>
        /// <param name="birth">birth date.</param>
        /// <param name="eyeColor">actor's eye color.</param>
        public void AddActor(string first_name, string last_name, string nationality, string birth, string eyeColor)
        {
            if (first_name == string.Empty || last_name == string.Empty || nationality == string.Empty || birth == string.Empty || eyeColor == string.Empty)
            {
                throw new ArgumentException("One of the argument was not specified");
            }

            if (first_name == null || last_name == null || nationality == null || birth == null || eyeColor == null)
            {
                throw new ArgumentNullException("One of the argument wasn't specified correctly");
            }
            else
            {
                szinesz newActor = new szinesz()
                {
                    vezeteknev = last_name,
                    keresztnev = first_name,
                    sz_datum = DateTime.Parse(birth),
                    nemzetiseg = nationality,
                    szemszin = eyeColor,
                };
                this.actorrepo.Add(newActor);
            }
        }

        /// <summary>
        /// Add a new director to the database.
        /// </summary>
        /// <param name="first_name">director's first name.</param>
        /// <param name="last_name">director's last name.</param>
        /// <param name="nationality">director's nationality.</param>
        /// <param name="birth">bith date.</param>
        /// <param name="salary">director's salary.</param>
        public void AddDirector(string first_name, string last_name, string nationality, string birth, string salary)
        {
            if (first_name == string.Empty || last_name == string.Empty || nationality == string.Empty || birth == string.Empty || salary == string.Empty)
            {
                throw new ArgumentException("One of the argument was not specified");
            }

            if (first_name == null || last_name == null || nationality == null || birth == null || salary == null)
            {
                throw new ArgumentNullException("One of the argument wasn't specified correctly");
            }
            else
            {
                rendezo newDirector = new rendezo()
                {
                    vezeteknev = last_name,
                    keresztnev = first_name,
                    sz_datum = DateTime.Parse(birth),
                    nemzetiseg = nationality,
                    fizetes = int.Parse(salary),
                };
                this.directorrepo.Add(newDirector);
            }
        }

        /// <summary>
        /// Add new Movie to database.
        /// </summary>
        /// <param name="title">movie title.</param>
        /// <param name="releaseYear">release year.</param>
        /// <param name="length">movie length.</param>
        /// <param name="plot">plot.</param>
        /// <param name="universeName">universeName.</param>
        /// <param name="directorName">directorName.</param>
        /// <param name="incoming">incoming value.</param>
        public void AddMovie(string title, string releaseYear, string length, string plot, string universeName, string directorName, string incoming)
        {
            if (title == string.Empty || releaseYear == string.Empty || length == string.Empty || plot == string.Empty || universeName == string.Empty || directorName == string.Empty || incoming == string.Empty)
            {
                throw new ArgumentException("One of the argument was not specified");
            }

            if (title == null || releaseYear == null || length == null || plot == null || universeName == null || directorName == null || incoming == null)
            {
                throw new ArgumentNullException("One of the argument wasn't specified correctly");
            }
            else
            {
                var director = this.directorrepo.GetAll().Single(x => x.keresztnev + " " + x.vezeteknev == directorName);
                var universe = this.universerepo.GetAll().Single(x => x.nev == universeName);

                film movie = new film()
                {
                    bevetel = int.Parse(incoming),
                    cim = title,
                    cselekmeny = plot,
                    hossz = int.Parse(length),
                    kijovetel = DateTime.Parse(releaseYear),
                    rendezoid = director.rendezoid,
                    univerzumid = universe.univerzumid,
                };
                this.movierepo.Add(movie);
            }
        }

        /// <summary>
        /// Add new role to the database.
        /// </summary>
        /// <param name="movieName">movieName.</param>
        /// <param name="actorName">actorName.</param>
        /// <param name="role">role.</param>
        public void AddRole(string movieName, string actorName, string role)
        {
            if (movieName == string.Empty || actorName == string.Empty || role == string.Empty)
            {
                throw new ArgumentException("One of the argument was not specified");
            }

            if (movieName == null || actorName == null || role == null)
            {
                throw new ArgumentNullException("One of the argument wasn't specified correctly");
            }
            else
            {
                var movie = this.movierepo.GetAll().Single(x => x.cim == movieName);
                var actor = this.actorrepo.GetAll().Single(x => x.keresztnev + " " + x.vezeteknev == actorName);

                szereples newRole = new szereples()
                {
                    filmid = movie.filmid,
                    szineszid = actor.szineszid,
                    szerep = role,
                };
                this.rolerepo.Add(newRole);
            }
        }

        /// <summary>
        /// Delete an actor.
        /// </summary>
        /// <param name="name">name.</param>
        public void DeleteActor(string name)
        {
            this.actorrepo.DeleteActor(name);
        }

        /// <summary>
        /// Delete a movie.
        /// </summary>
        /// <param name="title">title.</param>
        public void DeleteMovie(string title)
        {
            this.movierepo.DeleteMovie(title);
        }

        /// <summary>
        /// Delete a director.
        /// </summary>
        /// <param name="name">name.</param>
        public void DeleteDirector(string name)
        {
            this.directorrepo.DeleteDirector(name);
        }

        /// <summary>
        /// Delete a role.
        /// </summary>
        /// <param name="movieTitle">movieTitle.</param>
        /// <param name="actorName">actorName.</param>
        public void DeleteRole(string movieTitle, string actorName)
        {
            this.rolerepo.DeleteRole(movieTitle, actorName);
        }

        /// <summary>
        /// Lists the actors.
        /// </summary>
        /// <returns>an actor list.</returns>
        public List<szinesz> ListActors()
        {
            return this.actorrepo.GetAll();
        }

        /// <summary>
        /// Lists the directors.
        /// </summary>
        /// <returns>a director list.</returns>
        public List<rendezo> ListDirectors()
        {
            return this.directorrepo.GetAll();
        }

        /// <summary>
        /// Lists the movies.
        /// </summary>
        /// <returns>a movie list.</returns>
        public List<film> ListMovies()
        {
            return this.movierepo.GetAll();
        }

        /// <summary>
        /// Lists the roles.
        /// </summary>
        /// <returns>a role list.</returns>
        public List<szereples> ListRoles()
        {
            return this.rolerepo.GetAll();
        }

        /// <summary>
        /// Lists the movies with title.
        /// </summary>
        /// <returns>string list of movie titles.</returns>
        public List<string> ListMoviesWithTitle()
        {
            return this.movierepo.ListMoviesWithTitle();
        }

        /// <summary>
        /// Lists the directors with names.
        /// </summary>
        /// <returns>list of director names.</returns>
        public List<string> ListDirectorsWithNames()
        {
            return this.directorrepo.ListDirectorsWithNames();
        }

        /// <summary>
        /// Lists the roles.
        /// </summary>
        /// <returns>list of the roles.</returns>
        public List<string> ListRoleEntitiesWithNames()
        {
            return this.rolerepo.ListRoleEntitiesWithNames();
        }

        /// <summary>
        /// Lists actors with names.
        /// </summary>
        /// <returns>list of actor names.</returns>
        public List<string> ListActorsWithNames()
        {
            return this.actorrepo.ListActorsWithNames();
        }

        /// <summary>
        /// Update the actor's last name.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="newLastName">newLastName.</param>
        public void UpdateActorLastName(string name, string newLastName)
        {
            this.actorrepo.UpdateActorLastName(name, newLastName);
        }

        /// <summary>
        /// Update the given director's salary.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="newSalary">newSalary.</param>
        public void UpdateDirectorSalary(string name, string newSalary)
        {
            this.directorrepo.UpdateDirectorSalary(name, newSalary);
        }

        /// <summary>
        /// Update movie title.
        /// </summary>
        /// <param name="oldTitle">oldTitle.</param>
        /// <param name="newTitle">newTitle.</param>
        public void UpdateMovieTitle(string oldTitle, string newTitle)
        {
            this.movierepo.UpdateMovieTitle(oldTitle, newTitle);
        }

        /// <summary>
        /// Gives back an actor's movies.
        /// </summary>
        /// <param name="name">name.</param>
        /// <returns>string list of movies.</returns>
        public List<string> GivenActorMovies(string name)
        {
            List<string> givenActorMovies = new List<string>();

            var actor = from act in this.actorrepo.GetAll()
                            join role in this.rolerepo.GetAll()
                            on act.szineszid equals role.szineszid
                            join movie in this.movierepo.GetAll()
                            on role.filmid equals movie.filmid
                            where act.keresztnev + " " + act.vezeteknev == name
                            select movie.cim;

            foreach (string item in actor)
                {
                    givenActorMovies.Add(item);
                }

            return givenActorMovies;
        }

        /// <summary>
        /// Gives back the average incoming group by universe.
        /// </summary>
        /// <returns>list of universes with average incoming.</returns>
        public List<string> AverageIncomingGroupByUniverse()
        {
            List<string> avInc = new List<string>();

            var result = from movie in this.movierepo.GetAll()
                         group movie by movie.univerzumid into g1
                         select new
                         {
                             Universe = g1.Key,
                             AverageIncoming = g1.Average(x => x.bevetel),
                         };

            foreach (var item in result)
            {
                string name = " ";
                foreach (var universe in this.universerepo.GetAll())
                {
                    if (item.Universe == universe.univerzumid)
                    {
                        name = universe.nev;
                    }
                }

                avInc.Add(name);
                avInc.Add(item.AverageIncoming.ToString());
            }

            return avInc;
        }

        /// <summary>
        /// Gives back how many movies were the actors in.
        /// </summary>
        /// <returns>list of actors in how many movies.</returns>
        public List<string> ActorsInHowManyMovies()
        {
            List<string> act_s = new List<string>();

            var result = from role in this.rolerepo.GetAll()
                         group role by role.szineszid into g
                         select new
                         {
                             act = g.Key,
                             Count = g.Where(x => x.szineszid == g.Key).Count(),
                         };

            foreach (var item in result)
            {
                string name = " ";
                foreach (var actor in this.actorrepo.GetAll())
                {
                    if (item.act == actor.szineszid)
                    {
                        name = actor.keresztnev + " " + actor.vezeteknev;
                    }
                }

                act_s.Add(name);
                act_s.Add(item.Count.ToString());
            }

            return act_s;
        }

        /// <summary>
        /// Gives back the actor with the specified id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>an actor.</returns>
        public szinesz GetOneActor(int id)
        {
            return this.actorrepo.GetOne(id);
        }

        /// <summary>
        /// Gives back the movie with the specified id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a movie.</returns>
        public film GetOneMovie(int id)
        {
            return this.movierepo.GetOne(id);
        }

        /// <summary>
        /// Gives back a role with the specified ids.
        /// </summary>
        /// <param name="movieid">movieid.</param>
        /// <param name="actorid">actorid.</param>
        /// <returns>a role.</returns>
        public szereples GetOneRole(int movieid, int actorid)
        {
            return this.rolerepo.GetOne(movieid, actorid);
        }

        /// <summary>
        /// Gives back a director with the specified id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>a director.</returns>
        public rendezo GetOneDirector(int id)
        {
            return this.directorrepo.GetOne(id);
        }

        /// <summary>
        /// Lists the universes.
        /// </summary>
        /// <returns>list of universes.</returns>
        public List<univerzum> ListUniverses()
        {
            return this.universerepo.GetAll();
        }
    }
}
