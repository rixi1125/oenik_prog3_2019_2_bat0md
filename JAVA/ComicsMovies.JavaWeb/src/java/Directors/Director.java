/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directors;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Beatrix
 */

public class Director implements Serializable {
    int rendezoid;
    
    @XmlElement
    String keresztnev;
    
    @XmlElement
    String vezeteknev;
    
    @XmlElement
    String nemzetiseg;
    
    @XmlElement
    String sz_datum;
    
    @XmlElement
    String fizetes;

    public Director() {
    }
    
    public Director(int rendezoid, String keresztnev, String vezeteknev, String nemzetiseg, String sz_datum, String fizetes) {
        this.rendezoid = rendezoid;
        this.keresztnev = keresztnev;
        this.vezeteknev = vezeteknev;
        this.nemzetiseg = nemzetiseg;
        this.sz_datum = sz_datum;
        this.fizetes = fizetes;
    }

    
    public int getRendezoid() {
        return rendezoid;
    }

    public String getKeresztnev() {
        return keresztnev;
    }

    public String getVezeteknev() {
        return vezeteknev;
    }

    public String getNemzetiseg() {
        return nemzetiseg;
    }

    public String getSz_datum() {
        return sz_datum;
    }

    public String getFizetes() {
        return fizetes;
    }
    
    
}
