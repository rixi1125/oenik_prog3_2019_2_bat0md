/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directors;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Beatrix
 */
public class Repository {
    private static Repository instance;
    
    public static Repository getInstance(){
        if (instance == null) {
            instance = new Repository();
        }
        return instance;
    }
    
    List<Director> directors;

    public List<Director> getDirectors() {
        return directors;
    }

    public Repository() {
        directors = new ArrayList<>();
        Director d1 = new Director(1, "Jon","Favreu", "amerikai", "1966.10.19", "500000");
        Director d2 = new Director(2, "Louis","Leterrier", "francia", "1973.06.17", "65000");
        Director d3 = new Director(3, "Kenneth","Branagh", "brit", "1982.04.05", "440000");
        Director d4 = new Director(4, "Joe","Johnston", "amerikai", "1950.05.13", "768000");
        Director d5 = new Director(5, "Joss","Whedon", "amerikai", "1964.06.23", "345400");
        Director d6 = new Director(6, "Shane","Black", "amerikai", "1961.12.16", "1000000");
        Director d7 = new Director(7, "Alan","Taylor", "amerikai", "1965.03.02", "570200");
        Director d8 = new Director (8, "Anthony","Russo", "amerikai", "1970.02.03", "1000000");
        Director d9 = new Director(9, "James","Gunn", "amerikai", "1966.08.05", "987000");
        Director d10 = new Director(10, "Edgar","Wright", "angol", "1974.04.18", "567300");
        
        directors.add(d1);
        directors.add(d2);
        directors.add(d3);
        directors.add(d4);
        directors.add(d5);
        directors.add(d6);
        directors.add(d7);
        directors.add(d8);
        directors.add(d9);
        directors.add(d10);
    }
    
    
}
