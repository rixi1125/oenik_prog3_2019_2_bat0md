/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Beatrix
 */
public class FilterDirector {
    boolean nameFilter = false;
    String nameSubstring;
    
    boolean salaryFilter = false;
    int salarySubstring;
    
    public FilterDirector setNameSubstring(String name){
        nameFilter = true;
        nameSubstring = name;
        return this;
    }
    
    public FilterDirector setSalarySubstring(int sal){
        salaryFilter = true;
        salarySubstring = sal;
        return this;
    }
    
    public List<Director> Filter(Collection<Director> directors){
        List<Director> filtered = new ArrayList<Director>();
        
        for (Director director : directors) {
            int pez = Integer.parseInt(director.fizetes);
            int pez1 = salarySubstring;

            if((!nameFilter || director.keresztnev.contains(nameSubstring) || director.vezeteknev.contains(nameSubstring)) &&
                    (!salaryFilter || (pez >= pez1))){
                filtered.add(director);
            }
        }
        return filtered;
    }
}
