/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directors;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Beatrix
 */

@XmlRootElement(name = "RendezoCsoport")
@XmlAccessorType(XmlAccessType.FIELD)
public class DirectorGroup {
    
    @XmlElementWrapper(name = "rendezok")
    List<Director> directors;

    public List<Director> getDirectors() {
        return directors;
    }

    public DirectorGroup(List<Director> directors) {
        this.directors = directors;
    }

    public DirectorGroup() {
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }
    
    
}
