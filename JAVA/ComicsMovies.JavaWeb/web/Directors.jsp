<%-- 
    Document   : Directors
    Created on : 2019.11.27., 14:44:45
    Author     : Beatrix
--%>

<%@page import="java.util.List"%>
<%@page import="Directors.FilterDirector"%>
<%@page import="Directors.Director"%>
<%@page import="Directors.Repository"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Rendezők listája</title>
    </head>
    <body>
        <h1>Rendezők listája</h1>
        <form action="Directors.jsp">
            Rendező neve: <input type="text" name="directorname"/><br>
            Rendező fizetése: <input type="number" name="directorsalary"/><br>
            <input type="submit" values="szűrés"/>
        </form>
            <%
            FilterDirector filter =  new FilterDirector();
            if (request.getParameter("directorname") != null && request.getParameter("directorname").trim() != "" ) {
                    filter.setNameSubstring(request.getParameter("directorname"));
                }
            if (request.getParameter("directorsalary") != null && request.getParameter("directorsalary").trim() != "") {
                    filter.setSalarySubstring(Integer.parseInt(request.getParameter("directorsalary")));
                }
            
                List<Director> filtered = filter.Filter(Repository.getInstance().getDirectors());
                if (filtered.size() != 0) {
            %>
            <table style="border: 1px solid black;">
                <tr>
                    <th>Név</th><th>Nemzetiség</th><th>Születésnap</th><th>Fizetés</th>
                </tr>
                <% 
                int j =0;
                
                for (Director d : filtered) {
                        j++;
                %>
                <tr>
                    <td>
                        <%= d.getKeresztnev() + " " + d.getVezeteknev()%>
                    </td>
                    <td>
                        <%= d.getNemzetiseg()%>
                    </td>
                    <td>
                        <%= d.getSz_datum()%>
                    </td>
                    <td>
                        <%= d.getFizetes()%>
                    </td>
                </tr>
                <%
                }    
                %>
            </table>
            <% 
            }
            %>
    </body>
</html>
